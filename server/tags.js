const _ = require('lodash');

const ideas = require('lemon/src/database/ideas');
const links = require('lemon/src/database/links');
const { Subgraph, search } = require('lemon/src/database/subgraphs');

const { PromiseAllValues } = require('./utils');

exports.rest = (router, contexts) => {
	const root = '/tags';

	/** QUERY */
	router.get(root, (req, res) => {
		getTags(contexts)
			.then((list) => { res.json({ list }); })
			.catch((err) => { console.error(err); res.status(500).json({ message: err.message }); });
	});
};

exports.getTags = getTags;
exports.ensureTags = ensureTags;

// just get all the tags; this is useful for autocompleters / pickers
function getTags(contexts, { id = undefined } = {}) {
	const sg = new Subgraph();
	// the thing we are looking for
	sg.addVertex('filler', undefined, { name: 'tag' });
	// property def
	sg.addVertex('id', contexts.tags, { name: 'tagsDef' });
	// property def
	sg.addEdge('tag', 'typeOf', 'tagsDef');

	// we we have a task id, then only look for tags that belong to that task
	if (id) {
		sg.addVertex('id', id, { name: 'task' });
		sg.addEdge('task', 'property', 'tag');
	}

	return search(sg)
		.then((sgResults) => Promise.all(sgResults.map((sgResult) => PromiseAllValues({
			id: sgResult.getIdea('tag').id,
			tagData: sgResult.getData('tag'),
		}))))
		.then((list) => list.map(({ tagData, ...tag }) => ({
			...tag,
			...tagData,
		})));
}

/**
	@param {string} task.id - id of the task, so we can load the idea
	@param {Array<string>} task.tags - list of raw labels
*/
async function ensureTags(contexts, task) {
	const [all, curr] = await Promise.all([
		getTags(contexts),
		getTags(contexts, { id: task.id }),
	]);

	// add the target tags that are not in the current list
	const toAdd = _.uniq(task.tags.filter((label) => !curr.some((t) => t.label === label)));
	// split add into: update tags that already exist
	const toUpdate = toAdd.map((label) => all.find((t) => t.label === label)).filter((t) => !!t);
	// split add into: create tags that do not exist
	const toCreate = toAdd.filter((label) => !all.some((t) => t.label === label));

	// remove the current items that are not in the target list
	const toRemove = curr.filter(({ label }) => !task.tags.includes(label));

	return Promise.all([
		createTags(contexts, task.id, toCreate),
		linkTags(task.id, toUpdate),
		unlinkTags(task.id, toRemove),
	]);
}

/**
	@param {Array<string>} tags - list of raw labels
*/
function createTags(contexts, id, tags) {
	const now = new Date().toISOString();
	const tagName = (label) => `tag$${label}`;

	const verts = tags.reduce((ret, label) => {
		ret[tagName(label)] = {
			created: now,
			updated: now,
			label,
		};
		return ret;
	}, {});
	const edges = [];
	tags.forEach((label) => {
		edges.push([tagName(label), 'typeOf', contexts.tags]);
		edges.push([ideas.proxy(id), 'property', tagName(label)]);
	});

	return ideas.createGraph(verts, edges);
}

/**
	@param {Array<Object>} tags - list of tag data { id, label, created, updated }
*/
async function linkTags(id, tags) {
	const idea = ideas.proxy(id);
	const linkProperty = links.get('property');
	const now = new Date().toISOString();
	await Promise.all(tags.map((tag) => (
		idea.addLink(linkProperty, tag)
	)));
	await ideas.save(idea);
	await Promise.all(tags.map(({ id: tagId, ...tagData }) => (
		ideas.proxy(tagId)
			.setData({ ...tagData, updated: now })
			.then(() => ideas.save(tagId))
	)));
}

/**
	@param {Array<Object>} tags - list of tag data { id, label, created, updated }
*/
async function unlinkTags(id, tags) {
	const idea = ideas.proxy(id);
	const linkProperty = links.get('property');
	await Promise.all(tags.map((tag) => (
		idea.removeLink(linkProperty, tag)
			// if the tag isn't used anymore, then delete it
			.then(() => ideas.proxy(tag.id).getLinksOfType(linkProperty.opposite))
			.then((l) => (l.length === 0 ? ideas.delete(tag.id) : Promise.resolve()))
	)));
	await ideas.save(idea);
	await Promise.all(tags.map((tag) => (
		ideas.save(tag.id)
	)));
}
