const { zipObject, toPairs } = require('lodash');

exports.PromiseAllValues = async function PromiseAllValues(object) {
	// const keys = Object.keys(object);
	// const values = Object.values(object);
	const { keys, values } = toPairs(object).reduce((ret, [key, value]) => {
		ret.keys.push(key);
		ret.values.push(value);
		return ret;
	}, { keys: [], values: [] });
	return zipObject(keys, await Promise.all(values));
};
