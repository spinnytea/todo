const bodyParser = require('body-parser');
const { uniqBy } = require('lodash');

const config = require('lemon/src/config');
const ideas = require('lemon/src/database/ideas');
const links = require('lemon/src/database/links');
const { Subgraph, search, rewrite, destroy } = require('lemon/src/database/subgraphs');

const { getTags, ensureTags } = require('./tags');
const { PromiseAllValues } = require('./utils');

exports.rest = (router, contexts) => {
	const root = '/tasks';
	const specific = `${root}/:id`;

	/** QUERY */
	router.get(root, (req, res) => {
		let promise;

		if (req.query.query) {
			// REVIEW can we simplify this to search by "--property-> 'substring in value or label'"
			//  - this query string could be a separate vertex (it doesn't have to be specifically name or description or tag)
			let namePromise;
			let descPromise;
			let tagPromise;

			if (req.query.summary === 'true') {
				namePromise = getSummaries(contexts, { ...req.query, query: undefined, name: req.query.query });
				descPromise = getSummaries(contexts, { ...req.query, query: undefined, description: req.query.query });
				tagPromise = Promise.resolve([]);
			}
			else {
				namePromise = searchTask(contexts, { ...req.query, query: undefined, name: req.query.query });
				descPromise = searchTask(contexts, { ...req.query, query: undefined, description: req.query.query });
				tagPromise = searchTask(contexts, { ...req.query, query: undefined, tag: req.query.query });
			}

			promise = Promise.all([
				namePromise,
				descPromise,
				tagPromise,
			]).then(([
				nameList,
				descList,
				tagList,
			]) => (
				uniqBy(nameList.concat(descList).concat(tagList), (task) => task.id)
			));
		}
		else {
			// eslint-disable-next-line no-lonely-if
			if (req.query.summary === 'true') {
				promise = getSummaries(contexts);
			}
			else {
				promise = searchTask(contexts, req.query);
			}
		}

		promise
			.then((list) => { res.json({ list }); })
			.catch((err) => { console.error(err); res.status(500).json({ message: err.message }); });
	});

	/** CREATE */
	router.post(root, bodyParser.json(), (req, res) => {
		createTask(contexts, req.body)
			.then((data) => { res.json(data); config.save(); })
			.catch((err) => { console.error(err); res.status(500).json({ message: err.message }); });
	});

	/** UPDATE */
	router.put(specific, bodyParser.json(), (req, res) => {
		const task = req.body;
		if (task.id !== req.params.id) {
			const message = `request (${req.params.id}) and body (${task.id}) must have the same ids`;
			console.error(new Error(message));
			res.status(500).json({ message });
		}
		else {
			updateTask(contexts, task)
				.then((data) => { res.json(data); config.save(); })
				.catch((err) => { console.error(err); res.status(500).json({ message: err.message }); });
		}
	});

	/** DELETE */
	router.delete(specific, (req, res) => {
		const taskId = req.params.id;
		if (!taskId) {
			const message = 'missing task id in request';
			console.error(new Error(message));
			res.status(500).json({ message });
		}
		else {
			deleteTask(contexts, taskId)
				.then(() => { res.status(204).json(); config.save(); })
				.catch((err) => { console.error(err); res.status(500).json({ message: err.message }); });
		}
	});
};

exports.getSummaries = getSummaries;
exports.createTask = createTask;
exports.searchTask = searchTask;
exports.updateTask = updateTask;
exports.deleteTask = deleteTask;

function getSummaries(contexts, query = {}) {
	const sg = new Subgraph();
	if (query.id) {
		sg.addVertex('id', query.id, { name: 'task' });
	}
	else {
		sg.addVertex('filler', undefined, { name: 'task' });
	}
	// properties
	if (query.name) {
		sg.addVertex('substring', { value: query.name, path: 'value' }, { name: 'name' });
	}
	else {
		sg.addVertex('filler', undefined, { name: 'name' });
	}
	if (query.description) {
		sg.addVertex('substring', { value: query.description, path: 'value' }, { name: 'description' });
	}
	else {
		sg.addVertex('filler', undefined, { name: 'description' });
	}
	sg.addVertex('filler', undefined, { name: 'type' });
	// property defs
	sg.addVertex('id', contexts.task, { name: 'taskDef' });
	sg.addVertex('id', contexts.name, { name: 'nameDef' });
	sg.addVertex('id', contexts.description, { name: 'descriptionDef' });
	sg.addVertex('id', contexts.type, { name: 'typeDef' });
	// property defs
	sg.addEdge('task', 'typeOf', 'taskDef');
	sg.addEdge('name', 'typeOf', 'nameDef');
	sg.addEdge('description', 'typeOf', 'descriptionDef');
	sg.addEdge('type', 'typeOf', 'typeDef');
	// properties
	sg.addEdge('task', 'property', 'name');
	sg.addEdge('task', 'property', 'description');
	sg.addEdge('task', 'property', 'type');

	return search(sg)
		.then((sgResults) => Promise.all(sgResults.map((sgResult) => PromiseAllValues({
			id: sgResult.getIdea('task').id,
			name: sgResult.getData('name'),
			description: sgResult.getData('description'),
			type: sgResult.getIdea('type').id,
		}))))
		.then((list) => list.map((task) => ({
			...task,
			name: task.name.value,
			description: task.description?.value,
		})));
}

function createTask(contexts, data) {
	data.created = new Date().toISOString();
	data.updated = data.created;

	const verts = {
		task: { created: data.created, updated: data.updated },
		name: { value: data.name || '' },
		description: { value: data.description || '' },
	};
	const edges = [
		['task', 'typeOf', contexts.task],
		['name', 'typeOf', contexts.name],
		['description', 'typeOf', contexts.description],
		['task', 'property', 'name'],
		['task', 'property', 'description'],
		['task', 'property', ideas.proxy(data.priority)],
		['task', 'property', ideas.proxy(data.status)],
		['task', 'property', ideas.proxy(data.type)],
	];
	if (data.parent) {
		edges.push([ideas.proxy(data.parent.id), 'taskChild', 'task']);
	}

	return ideas.createGraph(verts, edges)
		.then(async () => {
			data.id = verts.task.id;
			await ensureTags(contexts, data);
			return data;
		});
}

function searchTask(contexts, query = {}) {
	const sg = new Subgraph();
	if (query.id) {
		sg.addVertex('id', query.id, { name: 'task' });
	}
	else {
		sg.addVertex('filler', undefined, { name: 'task' });
	}
	// properties
	if (query.name) {
		sg.addVertex('substring', { value: query.name, path: 'value' }, { name: 'name' });
	}
	else {
		sg.addVertex('filler', undefined, { name: 'name' });
	}
	if (query.description) {
		sg.addVertex('substring', { value: query.description, path: 'value' }, { name: 'description' });
	}
	else {
		sg.addVertex('filler', undefined, { name: 'description' });
	}
	sg.addVertex('filler', undefined, { name: 'priority' });
	if (query.statuses) {
		sg.addVertex('id', query.statuses.split(','), { name: 'status', orData: true });
	}
	else {
		sg.addVertex('filler', undefined, { name: 'status' });
	}
	sg.addVertex('filler', undefined, { name: 'type' });
	// property defs
	sg.addVertex('id', contexts.task, { name: 'taskDef' });
	sg.addVertex('id', contexts.name, { name: 'nameDef' });
	sg.addVertex('id', contexts.description, { name: 'descriptionDef' });
	sg.addVertex('id', contexts.priority, { name: 'priorityDef' });
	sg.addVertex('id', contexts.status, { name: 'statusDef' });
	sg.addVertex('id', contexts.type, { name: 'typeDef' });
	// property defs
	sg.addEdge('task', 'typeOf', 'taskDef');
	sg.addEdge('name', 'typeOf', 'nameDef');
	sg.addEdge('description', 'typeOf', 'descriptionDef');
	sg.addEdge('priority', 'typeOf', 'priorityDef');
	sg.addEdge('status', 'typeOf', 'statusDef');
	sg.addEdge('type', 'typeOf', 'typeDef');
	// properties
	sg.addEdge('task', 'property', 'name');
	sg.addEdge('task', 'property', 'description');
	sg.addEdge('task', 'property', 'priority');
	sg.addEdge('task', 'property', 'status');
	sg.addEdge('task', 'property', 'type');

	if (query.parentId) {
		sg.addVertex('id', query.parentId, { name: 'parent' });
		sg.addEdge('parent', 'typeOf', 'taskDef');
		sg.addEdge('parent', 'taskChild', 'task');
	}

	if (query.childId) {
		sg.addVertex('id', query.childId, { name: 'child' });
		sg.addEdge('child', 'typeOf', 'taskDef');
		sg.addEdge('child', 'taskChild-opp', 'task');
	}

	if (query.tag) {
		sg.addVertex('id', contexts.tags, { name: 'tagsDef' });
		sg.addVertex('substring', { value: query.tag, path: 'label' }, { name: 'tag' });
		sg.addEdge('tag', 'typeOf', 'tagsDef');
		sg.addEdge('task', 'property', 'tag');
	}

	return search(sg)
		.then((sgResults) => Promise.all(sgResults.map((sgResult) => PromiseAllValues({
			id: sgResult.getIdea('task').id,
			parent: sgResult.getIdea('task')
				.getLinksOfType(links.get('taskChild-opp'))
				.then((list) => (list?.length ? getSummaries(contexts, { id: list[0].id }) : undefined))
				.then((list) => (list?.length ? list[0] : undefined)),
			taskData: sgResult.getData('task'),
			name: sgResult.getData('name'),
			description: sgResult.getData('description'),
			priority: sgResult.getIdea('priority').id,
			status: sgResult.getIdea('status').id,
			tags: getTags(contexts, { id: sgResult.getIdea('task').id }),
			type: sgResult.getIdea('type').id,
		}))))
		.then((list) => list.map(({ taskData, ...task }) => ({
			...task,
			...taskData,
			name: task.name.value,
			description: task.description?.value,
			tags: task.tags.map(({ label }) => label),
		})));
}

function updateTask(contexts, task) {
	const sg = new Subgraph();
	sg.addVertex('id', task.id, { name: 'task', transitionable: true });
	// properties
	sg.addVertex('filler', undefined, { name: 'name', transitionable: true });
	sg.addVertex('filler', undefined, { name: 'description', transitionable: true });
	sg.addVertex('filler', undefined, { name: 'priority' });
	sg.addVertex('filler', undefined, { name: 'status' });
	sg.addVertex('filler', undefined, { name: 'type' });
	// property defs
	sg.addVertex('id', contexts.task, { name: 'taskDef' });
	sg.addVertex('id', contexts.name, { name: 'nameDef' });
	sg.addVertex('id', contexts.description, { name: 'descriptionDef' });
	sg.addVertex('id', contexts.priority, { name: 'priorityDef' });
	sg.addVertex('id', contexts.status, { name: 'statusDef' });
	sg.addVertex('id', contexts.type, { name: 'typeDef' });
	// property defs
	sg.addEdge('task', 'typeOf', 'taskDef');
	sg.addEdge('name', 'typeOf', 'nameDef');
	sg.addEdge('description', 'typeOf', 'descriptionDef');
	sg.addEdge('priority', 'typeOf', 'priorityDef');
	sg.addEdge('status', 'typeOf', 'statusDef');
	sg.addEdge('type', 'typeOf', 'typeDef');
	// properties
	sg.addEdge('task', 'property', 'name');
	sg.addEdge('task', 'property', 'description');
	sg.addEdge('task', 'property', 'priority', { name: 'edge-priority', transitionable: true });
	sg.addEdge('task', 'property', 'status', { name: 'edge-status', transitionable: true });
	sg.addEdge('task', 'property', 'type', { name: 'edge-type', transitionable: true });
	// new enums
	sg.addVertex('id', task.priority, { name: 'new-priority' });
	sg.addVertex('id', task.status, { name: 'new-status' });
	sg.addVertex('id', task.type, { name: 'new-type' });

	function handleParent() {
		const parentLink = links.get('taskChild-opp');
		// get direct parent (ignore transitive)
		const taskIdea = ideas.proxy(task.id);
		const newParentIdea = (task.parent?.id ? ideas.proxy(task.parent.id) : undefined);
		return taskIdea.getLinksOfType(parentLink).then(([oldParentIdea]) => {
			// did not exist before, does not exist now
			if (!newParentIdea && !oldParentIdea) return Promise.resolve();
			// exists and is unchanged
			if (newParentIdea?.id === oldParentIdea?.id) return Promise.resolve();

			return Promise.all([
				oldParentIdea && taskIdea.removeLink(parentLink, oldParentIdea),
				newParentIdea && taskIdea.addLink(parentLink, newParentIdea),
				oldParentIdea && ideas.save(oldParentIdea),
				newParentIdea && ideas.save(newParentIdea),
				ideas.save(taskIdea),
			]);
		});
	}

	function handleTags() {
		return ensureTags(contexts, task);
	}

	return search(sg)
		.then((list) => {
			if (list?.length !== 1) {
				return Promise.reject(new Error(`Could not find (1) task "${task.id}", instead found (${list?.length || 0}).`));
			}

			task.updated = new Date().toISOString();
			return rewrite(list[0], [
				{ vertexId: 'task', replace: { created: task.created, updated: task.updated } },
				{ vertexId: 'name', replace: (task.name ? { value: task.name } : null) },
				{ vertexId: 'description', replace: (task.description ? { value: task.description } : null) },
				{ edgeId: 'edge-priority', replaceDst: 'new-priority' },
				{ edgeId: 'edge-status', replaceDst: 'new-status' },
				{ edgeId: 'edge-type', replaceDst: 'new-type' },
			], true);
		})
		.then((result) => result.save())
		.then(handleParent)
		.then(handleTags)
		.then(() => task);
}

function deleteTask(contexts, taskId) {
	const sg = new Subgraph();
	sg.addVertex('id', taskId, { name: 'task' });
	// properties
	sg.addVertex('filler', undefined, { name: 'name' });
	sg.addVertex('filler', undefined, { name: 'description' });
	sg.addVertex('filler', undefined, { name: 'priority' });
	sg.addVertex('filler', undefined, { name: 'status' });
	sg.addVertex('filler', undefined, { name: 'type' });
	// property defs
	sg.addVertex('id', contexts.task, { name: 'taskDef' });
	sg.addVertex('id', contexts.name, { name: 'nameDef' });
	sg.addVertex('id', contexts.description, { name: 'descriptionDef' });
	sg.addVertex('id', contexts.priority, { name: 'priorityDef' });
	sg.addVertex('id', contexts.status, { name: 'statusDef' });
	sg.addVertex('id', contexts.type, { name: 'typeDef' });
	// property defs
	sg.addEdge('task', 'typeOf', 'taskDef');
	sg.addEdge('name', 'typeOf', 'nameDef');
	sg.addEdge('description', 'typeOf', 'descriptionDef');
	sg.addEdge('priority', 'typeOf', 'priorityDef');
	sg.addEdge('status', 'typeOf', 'statusDef');
	sg.addEdge('type', 'typeOf', 'typeDef');
	// properties
	sg.addEdge('task', 'property', 'name');
	sg.addEdge('task', 'property', 'description');
	sg.addEdge('task', 'property', 'priority', { name: 'edge-priority' });
	sg.addEdge('task', 'property', 'status', { name: 'edge-status' });
	sg.addEdge('task', 'property', 'type', { name: 'edge-type' });

	return search(sg)
		.then(async (list) => {
			if (list?.length !== 1) {
				return Promise.reject(new Error(`Could not find (1) task "${taskId}", instead found (${list?.length || 0}).`));
			}

			// remove all the tags from this task first
			await ensureTags(contexts, { id: taskId, tags: [] });

			// same as createTask (verts)
			const vertexIds = [
				'task',
				'name',
				'description',
			];

			return destroy(list[0], vertexIds)
				.then(() => ({ id: taskId }));
		});
}
