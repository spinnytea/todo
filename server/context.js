const config = require('lemon/src/config');
const ideas = require('lemon/src/database/ideas');
const links = require('lemon/src/database/links');

const enums = require('./enums');
const tags = require('./tags');
const tasks = require('./tasks');

let initialized;

/*
	part of the task/core idea
	----
	id: id, // self-explanatory, all IDs are truthy
	created: ISO String, // when the ticket was created
	updated: ISO String, // when the ticket was last updated

	props with dedicated values
	----
	name: '', // summary text, title
	description: '', // long form
	tags, // multiple links, categories

	pure links
	----
	parent, // null, undefined; truthy means it has a parent

	links to enums
	----
	type, // epic/task/bug etc
	status, // single link, have we started/finished/etc
	priority, // single link, how important is it (number, higher numbers at the top of the list, unbounded)
*/
config.onInit(() => {
	initialized = new Promise((resolve) => {
		ideas.context('Todo Root').then((todoRootContext) => {
			links.units.create('taskChild', { transitive: true });
			const contextLink = links.get('context');

			const todoContexts = {
				task: 'Todo Task',
				name: 'Todo Task Property: Name',
				description: 'Todo Task Property: Description',
				tags: 'Todo Task Property: Tags',
				type: 'Todo Enum: Type',
				status: 'Todo Enum: Status',
				priority: 'Todo Enum: Priority',
			};

			Promise.all(Array.from(Object.entries(todoContexts)).map(([key, name]) => (
				ideas.context(name)
					.then((proxy) => proxy.addLink(contextLink, todoRootContext))
					.then((proxy) => ideas.save(proxy))
					.then((proxy) => (todoContexts[key] = proxy))
			)))
				.then(() => ideas.save(todoRootContext))
				.then(() => config.save())
				.then(() => resolve(todoContexts));
		});
	});
});

exports.rest = (router) => {
	initialized.then((todoContexts) => {
		enums.rest(router, 'types', todoContexts.type);
		enums.rest(router, 'statuses', todoContexts.status);
		enums.rest(router, 'priorities', todoContexts.priority);

		tags.rest(router, todoContexts);

		tasks.rest(router, todoContexts);
	});

	return router;
};

exports.initialized = new Promise((resolve, reject) => {
	let retryCount = 10;
	function checkReady() {
		// if ready, then we are done
		if (initialized) {
			resolve(initialized);
			return;
		}

		// if not ready, check for too many tries
		retryCount -= 1;
		if (retryCount <= 0) {
			reject(new Error('initialized timed out'));
			return;
		}

		// not timed out, not ready, wait a bit, then check again
		setTimeout(checkReady, 10);
	}
	checkReady();
});
