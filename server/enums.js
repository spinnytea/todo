const bodyParser = require('body-parser');

const config = require('lemon/src/config');
const ideas = require('lemon/src/database/ideas');
const links = require('lemon/src/database/links');

exports.rest = (router, path, context) => {
	const root = `/${path}`;
	const specific = `${root}/:id`;
	const linkTypeOf = links.get('typeOf');

	/** QUERY */
	router.get(root, (req, res) => {
		context.getLinksOfType(linkTypeOf.opposite)
			.then((list) => Promise.all(list.map((idea) => idea.getData())))
			.then((list) => res.json({ list }))
			.catch((err) => { res.status(500).json({ message: err.message }); });
	});

	/** CREATE */
	router.post(root, bodyParser.json(), (req, res) => {
		const data = req.body;
		ideas.create().then((idea) => {
			data.id = idea.id;

			return idea.setData(data)
				.then(() => idea.addLink(linkTypeOf, context))
				.then(() => Promise.all([ideas.save(idea), ideas.save(context)]))
				.then(() => { res.json(data); config.save(); });
		}).catch((err) => { res.status(500).json({ message: err.message }); });
	});

	/** UPDATE */
	router.put(specific, bodyParser.json(), (req, res) => {
		const data = req.body;
		if (data.id !== req.params.id) {
			res.status(500).json({ message: `request (${req.params.id}) and body (${data.id}) must have the same ids` });
		}
		else {
			Promise.resolve(ideas.proxy(data.id))
				.then((idea) => idea.setData(data))
				.then((idea) => ideas.save(idea))
				.then(() => res.json(data))
				.catch((err) => { res.status(500).json({ message: err.message }); });
		}
	});

	/** COUNT ALL */
	// router.get(root + '/count', function countAll(req, res) {
	//   res.json(context.link(links.list.type_of.opposite).reduce(function(counts, idea) {
	//     counts[idea.id] = ideas.load(idea.id).link(link.opposite).length;
	//     return counts;
	//   }, {}));
	// });
};
