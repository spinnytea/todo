Todo
===

This is a sample use case of `lemon` database.
`lemon` is going to do more than just store and search graphs.
But this project doesn't need to do anything else, yet.

This may not be the best way to build a task database
(nay, it certainly isn't).
But the point is to showcase and try out different ways of representing information.


Setting up the project
===
1. download lemon
1. from with in lemon, run `npm link`
1. npm install
1. npm run server
1. npm start

For Testing
===
```js
fetch('/rest/types').then((res) => res.json()).then((...args) => console.log(...args)).catch((...err) => console.error(...err))
```