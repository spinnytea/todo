import React, { useState } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import { Typography } from '@material-ui/core';
import styled from 'styled-components';

import PageFooter from './components/PageFooter';
import { EnumList, useEnums } from './components';
import { Alert } from './components/mui';
import Splash from './routes/Splash';
import TasksPage from './routes/TasksPage';
import Dashboard from './routes/Dashboard';

const StyledTypography = styled(Typography)`
	color: ${(props) => props.theme.palette.text.primary};
	background-color: ${(props) => props.theme.palette.background.default};

	// weird complexity, but adds polish - ensures the footer is at the bottom
	// if we get rid of this, we can remove the section from ConnectedRoot
	min-height: 100vh;
	display: flex;
	flex-direction: column;
	section {
		flex: 1;
	}
`;

export default function ConnectedRoot() {
	const [error, setError] = useState();

	const types = useEnums('types', setError);
	const statuses = useEnums('statuses', setError);
	const priorities = useEnums('priorities', setError);

	const loading = !types.length || !statuses.length || !priorities.length;

	return (
		<StyledTypography variant="body1" component="div">
			<Alert message={error} />

			<section>
				<Routes>
					<Route path="/tasks" element={loading ? <Splash key="Splash" /> : <TasksPage />} />
					<Route path="/enums">
						<Route path="types" element={<EnumList key="EnumList-types" variant="types" />} />
						<Route path="statuses" element={<EnumList key="EnumList-statuses" variant="statuses" />} />
						<Route path="priorities" element={<EnumList key="EnumList-priorities" variant="priorities" />} />
					</Route>
					<Route path="/splash" element={<Splash />} />
					<Route path="/" element={loading ? <Splash key="Splash" /> : <Dashboard />} />
					<Route
						path="*"
						element={<Navigate to="/" replace />}
					/>
				</Routes>
			</section>

			<PageFooter />
		</StyledTypography>
	);
}
