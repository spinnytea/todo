import React, { useCallback, useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { TextField, Tooltip } from '@material-ui/core';
import styled from 'styled-components';
import _ from 'lodash';

import { StatusIcon } from './common';
import { PopperAutoWidth } from './mui';
import tasksService from '../services/tasksService';
import utils from '../utils';

const StyledTask = styled.div`
	flex: 1 1 auto;

	&.action {
		color: ${(props) => props.theme.palette.text.hint};
		flex: 1;
		text-align: center;
	}
`;

const StyledErrorText = styled.span`
	display: flex;
	align-items: center;
	justify-content: flex-start;

	.icon {
		margin-right: 0.5ch;
	}
`;

const AUTOCOMPLETE_QUERY_SIZE = 5;

function buildSuggestions(data, { filterSize }) {
	// we might get "exactly 5 results" with a size of "5", and hit more; but then we'll have "5 results" with a size of "10"
	//  - this may be a little wasteful, but it's stable / it terminates
	//  - the system doesn't count "possible matches given a more relaxed filter"
	//  - XXX the system doesn't differentiate between "search" and "filter" and "facet", it only narrows matches
	//  - but it doesn't need to; doing a "search + filter" is a human thing, a way of keep track while sifting through a lot of results
	//  - IDEA do we need to implement this more human approach (search + filter) or is it fine for the machine to be exhaustive in every case
	//  - there are a lot of early termination points (find the first best match, etc), pagination might just be an unnecessary extra step
	//  - unless we can get a global "do the full search, but start with your preferred filter" and then if that fails "fallback and do everything"
	//  - maybe that will lead to quicker results
	const suggestionsOtherDocCount = (data.length === filterSize);
	const newSuggestions = data.slice(0);

	if (suggestionsOtherDocCount) {
		newSuggestions.push({
			name: '- show more -',
			action: 'fetch-more-suggestions',
			disabled: true,
		});
	}

	return newSuggestions;
}

export default function TaskAutocomplete(props) {
	const {
		className,
		disabled,
		display = utils.nameToDisplay(props.name), // eslint-disable-line react/destructuring-assignment
		id = props.name, // eslint-disable-line react/destructuring-assignment
		name,
		required,
		value,
		valueChange,
	} = props;

	const [[filterValue, filterSize], setFetchData] = useState(['', AUTOCOMPLETE_QUERY_SIZE]);
	const [[isLoadingSuggestions, suggestions], setSuggestions] = useState(['', []]);

	const onAutocompleteSelect = useCallback((event, task) => {
		if (task) {
			if (valueChange) {
				valueChange([name, task, event]);
			}
			// reset state
			setFetchData(['', AUTOCOMPLETE_QUERY_SIZE]);
			setSuggestions(['', []]);
		}
	}, [valueChange, name]);

	// fetch suggestions
	useEffect(() => {
		if (filterValue) {
			let active = true;
			setSuggestions(['Pending...', []]);

			// debounce the request
			// we can't use TextFieldDebounced, because that doesn't give us a cancel when the input changes (between input change and state change)
			// we need to cancel as soon as the input changes
			let timeoutId = setTimeout(() => {
				if (!timeoutId) return;

				setSuggestions(['Loading...', []]);

				tasksService.list({ query: filterValue.trim() }).then((data) => {
					const newSuggestions = buildSuggestions(data.list, { filterSize });
					setSuggestions(['', newSuggestions]);
				}).catch((error) => {
					if (active) {
						const errorMessage = (
							<Tooltip title={error?.message}>
								<StyledErrorText>
									<StatusIcon severity="error" variant="outlined" color="error" className="icon" fontSize="small" />
									Search Failed
								</StyledErrorText>
							</Tooltip>
						);
						setSuggestions([errorMessage, []]);
					}
				});
			}, utils.SHORT_DELAY);

			return () => {
				clearTimeout(timeoutId);
				timeoutId = null;
				active = false;
			};
		}

		return undefined;
	}, [filterValue, filterSize]);

	const handleChange = useCallback(({ target: { value: newValue } }) => {
		setFetchData([newValue, AUTOCOMPLETE_QUERY_SIZE]);
	}, []);
	const handleInputChange = useCallback((event) => {
		if (valueChange && event && !event.target.value) {
			// we only want to clear the value if we "click on the `clearIndicator`", whose target could be button, svg, or path (or anything else)
			// but we don't want to clear if the user delete the value from the input
			// TODO we need a keyboard clear task, `clearOnEscape` isn't working
			if (event.target.tagName.toLowerCase() !== 'input') {
				valueChange([name, undefined, event]);
			}
		}
	}, [valueChange, name]);

	const onSuggestionClick = useCallback((ev, task) => {
		if (task.action === 'fetch-more-suggestions') {
			ev.stopPropagation();
			setFetchData([filterValue, filterSize + AUTOCOMPLETE_QUERY_SIZE]);
		}
	}, [filterValue, filterSize]);

	// Autocomplete complains if the `input` doesn't match any of the tasks
	//  - so IFF we don't match anything, then pretend the first item is a match
	const selectedTask = useMemo(() => {
		const v = filterValue.toLowerCase().trim();
		const idx = _.findIndex(suggestions, (task) => task.name?.toLowerCase().trim() === v);
		if (idx === -1) return suggestions[0];
		return suggestions[idx];
	}, [suggestions, filterValue]);

	return (
		<Autocomplete
			className={className}
			disabled={disabled}
			filterOptions={_.identity}
			getOptionLabel={(task) => (task.name || task)}
			getOptionSelected={(task) => (task === selectedTask)}
			loading={!!isLoadingSuggestions}
			loadingText={isLoadingSuggestions}
			open={filterValue !== ''}
			options={suggestions}
			value={value}
			onChange={onAutocompleteSelect}
			onInputChange={handleInputChange}
			onBlur={() => { setFetchData(['', AUTOCOMPLETE_QUERY_SIZE]); }}
			PopperComponent={PopperAutoWidth}
			renderInput={(params) => (
				<TextField
					{...params} // eslint-disable-line react/jsx-props-no-spreading
					fullWidth
					id={id}
					label={display}
					name={name}
					required={required}
					value={filterValue}
					onChange={handleChange}
					InputLabelProps={{
						htmlFor: id,
						shrink: true,
					}}
				/>
			)}
			renderOption={(task) => (
				<Tooltip title={task.description || <i>No Description</i>}>
					<StyledTask
						onClick={(ev) => { onSuggestionClick(ev, task); }}
						className={task.action ? 'action' : ''}
					>
						{task.name}
					</StyledTask>
				</Tooltip>
			)}
		/>
	);
}

TaskAutocomplete.propTypes = {
	// i/o
	value: PropTypes.any, // eslint-disable-line react/forbid-prop-types
	valueChange: PropTypes.func,

	// optional
	className: PropTypes.string,
	disabled: PropTypes.bool,
	display: PropTypes.string,
	id: PropTypes.string,
	name: PropTypes.string,
	required: PropTypes.bool,
};

TaskAutocomplete.defaultProps = {
	className: undefined,
	disabled: false,
	display: undefined,
	id: undefined,
	name: undefined,
	required: false,
	value: '',
	valueChange: null,
};
