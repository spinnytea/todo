import { useSelector } from 'react-redux';

export default function useTags() {
	return useSelector((state) => state.tasks.allTags)
		.map(({ label }) => label);
}
