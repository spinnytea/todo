import React from 'react';
import { Popper } from '@material-ui/core';

export default function PopperAutoWidth(props) {
	const minWidth = props?.style?.width || '25ch'; // eslint-disable-line react/destructuring-assignment, react/prop-types
	return (
		<Popper
			{...props} // eslint-disable-line react/jsx-props-no-spreading
			style={{ width: 'unset', minWidth, maxWidth: '80ch' }}
			placement="bottom-start"
		/>
	);
}
