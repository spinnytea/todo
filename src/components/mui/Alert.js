import React from 'react';
import PropTypes from 'prop-types';
import { Alert as MuiAlert } from '@material-ui/lab';

export default function Alert({ className, message, severity }) {
	if (!message) return null;

	return (
		<MuiAlert className={className} severity={severity}>
			{message}
		</MuiAlert>
	);
}

Alert.propTypes = {
	className: PropTypes.string,
	message: PropTypes.string,
	severity: PropTypes.oneOf(['info', 'success', 'warning', 'error']),
};

Alert.defaultProps = {
	className: undefined,
	message: undefined,
	severity: 'error',
};
