import React from 'react';
import PropTypes from 'prop-types';
import { Button as MuiButton, useTheme } from '@material-ui/core';
import styled from 'styled-components';

const StyledButton = styled(MuiButton)`
	&:not(:disabled).MuiButton-contained {
		color: ${(props) => props.$actionColor.contrastText};
		background-color: ${(props) => props.$actionColor.main};
	}
	&:not(:disabled).MuiButton-outlined {
		color: ${(props) => props.$actionColor.main};
		border-color: ${(props) => props.$actionColor.light};
	}
`;
export default function Button({ actionColor, actionLabel, className, disabled, type, variant, onClick }) {
	const theme = useTheme();
	const $actionColor = theme.palette[actionColor];
	return (
		<StyledButton
			className={className}
			type={type}
			variant={variant}
			disabled={disabled}
			onClick={onClick}
			aria-label={actionLabel}
			$actionColor={$actionColor}
		>
			{actionLabel}
		</StyledButton>
	);
}

Button.propTypes = {
	actionColor: PropTypes.oneOf(['primary', 'info', 'success', 'warning', 'error']),
	actionLabel: PropTypes.string.isRequired,
	className: PropTypes.string,
	disabled: PropTypes.bool,
	type: PropTypes.oneOf(['button', 'submit']),
	variant: PropTypes.oneOf(['contained', 'outlined']),

	onClick: PropTypes.func.isRequired,
};

Button.defaultProps = {
	actionColor: 'primary',
	className: null,
	disabled: false,
	type: 'button',
	variant: 'contained',
};
