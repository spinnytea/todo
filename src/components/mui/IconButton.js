import React from 'react';
import PropTypes from 'prop-types';
import { IconButton as MuiIconButton, Tooltip, useTheme } from '@material-ui/core';
import styled from 'styled-components';

import utils from '../../utils';

const StyledIconButton = styled(MuiIconButton)`
	&:not(:disabled) {
		svg {
			color: ${(props) => props.$actionColor};
		}
		.MuiTouchRipple-child {
			background-color: ${(props) => props.$actionColor};
		}
	}
`;

export default function IconButton({ actionColor, actionLabel, className, disabled, fontSize, Icon, onClick }) {
	const theme = useTheme();
	const $actionColor = (actionColor === 'action' ? theme.palette.action.active : theme.palette[actionColor].main);

	return (
		<Tooltip title={actionLabel} enterDelay={utils.SHORT_DELAY}>
			<StyledIconButton className={className} onClick={onClick} disabled={disabled} aria-label={actionLabel} $actionColor={$actionColor}>
				<Icon
					fontSize={fontSize}
				/>
			</StyledIconButton>
		</Tooltip>
	);
}

IconButton.propTypes = {
	actionColor: PropTypes.oneOf(['action', 'primary', 'info', 'success', 'warning', 'error']),
	actionLabel: PropTypes.string.isRequired,
	className: PropTypes.string,
	disabled: PropTypes.bool,
	fontSize: PropTypes.oneOf(['inherit', 'small', 'medium', 'large']),
	Icon: PropTypes.elementType.isRequired,
	onClick: PropTypes.func.isRequired,
};

IconButton.defaultProps = {
	actionColor: 'action',
	className: undefined,
	disabled: false,
	fontSize: undefined,
};
