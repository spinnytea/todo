import React from 'react';
import PropTypes from 'prop-types';
import { LinearProgress as MuiLinearProgress } from '@material-ui/core';
import styled from 'styled-components';

import { Paragraph } from '../elements';

const MuiLinearProgressPlaceholder = styled.div`
	height: 4px;
`;

export default function LinearProgress({ open }) {
	return (
		<Paragraph>
			{open ? (
				<MuiLinearProgress />
			) : (
				<MuiLinearProgressPlaceholder />
			)}
		</Paragraph>
	);
}

LinearProgress.propTypes = {
	open: PropTypes.bool.isRequired,
};

LinearProgress.defaultProps = {
};
