import React from 'react';
import PropTypes from 'prop-types';
import { Divider as MuiDivider } from '@material-ui/core';

export default function Divider({ orientation }) {
	if (orientation === 'vertical') {
		return (
			<MuiDivider aria-hidden="true" orientation="vertical" flexItem />
		);
	}

	return (
		<MuiDivider aria-hidden="true" />
	);
}

Divider.propTypes = {
	orientation: PropTypes.oneOf(['horizontal', 'vertical']).isRequired,
};
