import React from 'react';
import PropTypes from 'prop-types';
import { Dialog as MuiDialog } from '@material-ui/core';

export default function Dialog({ maxWidth, open, children, onMetaClose }) {
	return (
		<MuiDialog
			open={open}
			maxWidth={maxWidth}
			fullWidth
			onClose={onMetaClose}
		>
			{children}
		</MuiDialog>
	);
}

Dialog.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node,
	]).isRequired,
	maxWidth: PropTypes.oneOf(['xs', 'sm', 'md', 'lg']),
	open: PropTypes.bool,
	onMetaClose: PropTypes.func,
};

Dialog.defaultProps = {
	maxWidth: 'sm',
	open: false,
	onMetaClose: null,
};
