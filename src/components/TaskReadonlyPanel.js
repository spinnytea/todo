import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardContent, CardHeader, Tooltip, Typography } from '@material-ui/core';
import styled from 'styled-components';

import { Markdown, Tags } from './elements';
import { EnumAvatar, EnumIcon, useStatusCategory } from './enums';
import { CloseIcon, IconButton } from './mui';
import tasksService from '../services/tasksService';
import utils from '../utils';

const StyledCard = styled(Card)`
	border-left-width: ${(props) => props.theme.spacing(1)}px;
	border-left-style: solid;
	border-left-color: ${(props) => props.$statusCategory?.accentColor};
`;

const StyledName = styled(Typography)`
	display: flex;
	align-items: center;
	justify-content: flex-start;
	color: ${(props) => props.$statusCategory?.accentColor};

	.icon {
		margin-right: ${(props) => props.theme.spacing(1)}px;
	}
`;

const StyledDescription = styled(Markdown)`
	max-height: 5em;
	overflow: hidden scroll;

	border: 1px solid ${(props) => props.theme.palette.divider};
	color: ${(props) => props.theme.palette.text.secondary};
	padding: 0.25em;
	margin: 0.25em 0;
`;

/**
	This is just a tool to visualize the data.
	We probably won't use this in any way, and in the end nothing will be "readonly"
*/
export default function TaskReadonlyPanel({ onClose, task }) {
	const statusCategory = useStatusCategory(task);

	return (
		<StyledCard $statusCategory={statusCategory}>
			<CardHeader
				title={task.name}
				subheader={`Task ID: ${task.id}`}
				avatar={<EnumAvatar task={task} />}
				action={onClose ? (
					<IconButton actionLabel="Close" Icon={CloseIcon} onClick={onClose} />
				) : null}
			/>
			<CardContent>
				<StyledName variant="body1" $statusCategory={statusCategory} component="p">
					<EnumIcon task={task} variant="types" />
					<EnumIcon task={task} variant="statuses" />
					<EnumIcon task={task} variant="priorities" />
				</StyledName>
				{task.description ? (
					<StyledDescription>
						{task.description}
					</StyledDescription>
				) : (
					<Typography variant="body1" color="textSecondary" component="p">
						<i>No Description</i>
					</Typography>
				)}
				{task.parent ? (
					<StyledName variant="body1" color="textSecondary" component="p">
						<EnumIcon task={task.parent} variant="types" />
						<span className="separator" />

						<Tooltip title={task.parent.description || <i>No Description</i>}>
							<span>{task.parent.name}</span>
						</Tooltip>
					</StyledName>
				) : (
					<StyledName variant="body1" color="textSecondary" component="p">
						<i>No Parent</i>
					</StyledName>
				)}
				<Tags value={task.tags} readonly />
				<Typography variant="body1" color="textSecondary" component="p">
					Updated {utils.time.fromNow(task.updated)}
				</Typography>
				<Typography variant="body1" color="textSecondary" component="p">
					Created {utils.time.format(task.created)}
				</Typography>
			</CardContent>
		</StyledCard>
	);
}

TaskReadonlyPanel.propTypes = {
	onClose: PropTypes.func,
	task: tasksService.propTypes.isRequired,
};

TaskReadonlyPanel.defaultProps = {
	onClose: null,
};
