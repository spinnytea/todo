import { useEffect } from 'react';
import { useSelector } from 'react-redux';

import { enumActions } from '../../redux';

export default function useEnums(variant, setError) {
	const list = useSelector((state) => state.enums[variant]);

	useEffect(() => {
		// XXX this isn't perfect, but it works well enough
		//  - it might make multiple requests depending on the page
		if (!list.length) {
			enumActions.refresh(variant).catch(({ message }) => { if (setError) setError(message); });
		}
	}, [variant, setError, list]);

	return list;
}
