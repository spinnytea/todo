import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { Card, CardActions, CardContent, Typography } from '@material-ui/core';

import { Button } from '../mui';
import * as icons from '../mui/icons';
import { Checkbox, Input, Select, Textarea } from '../elements';
import enumsService from '../../services/enumsService';
import utils from '../../utils';

export default function EnumEditPanel({ disabled, item, onChange, onSubmit, variant }) {
	const hasCategories = (variant === 'statuses');

	const Icon = icons[item.icon];

	const handleChange = useCallback(([name, value]) => {
		if (name === 'default' && value === false) value = undefined;
		onChange({
			...item,
			[name]: value,
		});
	}, [onChange, item]);

	const handleSubmit = useCallback((event) => {
		event.preventDefault();
		event.stopPropagation();
		if (onSubmit) onSubmit(event, item);
	}, [onSubmit, item]);

	return (
		<Card component="form" onSubmit={handleSubmit}>
			<CardContent>
				<Typography variant="body2" color="textSecondary" component="p">
					{utils.nameToDisplay(variant)} ID: {item.id}
				</Typography>
				<Input id={`${variant}-${item.id}-name`} name="name" value={item.name} valueChange={handleChange} />
				<Select id={`${variant}-${item.id}-icon`} name="icon" value={item.icon} valueChange={handleChange} startAdornment={<Icon />}>
					{Array.from(Object.entries(icons)).map(([key]) => (
						<option key={key} value={key} aria-label={`Icon of ${key}`}>{key}</option>
					))}
				</Select>
				<Input id={`${variant}-${item.id}-order`} name="order" type="number" value={item.order} valueChange={handleChange} onSubmit={handleSubmit} />
				{hasCategories && (
					<Select id={`${variant}-${item.id}-category`} name="category" value={item.category} valueChange={handleChange}>
						{enumsService.statusCategories.map(({ id, display }) => (
							<option key={id} value={id} aria-label={`Category of ${display}`}>{display}</option>
						))}
					</Select>
				)}
				<Checkbox id={`${variant}-${item.id}-default`} name="default" value={item.default} valueChange={handleChange} />
				<Textarea
					id={`${variant}-${item.id}-description`}
					name="description"
					value={item.description}
					valueChange={handleChange}
					onSubmit={handleSubmit}
				/>
			</CardContent>
			<CardActions style={{ justifyContent: 'flex-end' }}>
				<Button type="submit" actionLabel="Save" onClick={handleSubmit} disabled={disabled} />
			</CardActions>
		</Card>
	);
}

EnumEditPanel.propTypes = {
	disabled: PropTypes.bool,
	item: PropTypes.exact({
		category: PropTypes.string,
		default: PropTypes.bool,
		description: PropTypes.string.isRequired,
		icon: PropTypes.string.isRequired,
		id: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired,
		order: PropTypes.number.isRequired,
	}).isRequired,
	onChange: PropTypes.func,
	onSubmit: PropTypes.func,
	variant: PropTypes.oneOf(['types', 'statuses', 'priorities']).isRequired,
};

EnumEditPanel.defaultProps = {
	disabled: false,
	onChange: null,
	onSubmit: null,
};
