import React from 'react';
import PropTypes from 'prop-types';
import { Tooltip } from '@material-ui/core';

import useEnum from './useEnum';
import * as icons from '../mui/icons';

export default function EnumIcon({ color, fontSize, task, variant }) {
	const item = useEnum(variant, task);
	const Icon = icons[item.icon];

	return (
		<Tooltip title={`${item.name}: ${item.description}`}>
			<Icon className="icon" color={color} fontSize={fontSize} />
		</Tooltip>
	);
}

EnumIcon.propTypes = {
	color: PropTypes.oneOf(['action', 'disabled', 'error', 'inherit', 'primary', 'secondary']),
	fontSize: PropTypes.oneOf(['inherit', 'small', 'medium', 'large']),
	task: PropTypes.shape({
		priority: PropTypes.string,
		status: PropTypes.string,
		type: PropTypes.string,
	}).isRequired,
	variant: PropTypes.oneOf(['types', 'statuses', 'priorities']).isRequired,
};

EnumIcon.defaultProps = {
	fontSize: 'medium',
	color: undefined,
};
