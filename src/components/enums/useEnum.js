import { useSelector } from 'react-redux';

const FIELD_MAP = {
	priorities: 'priority',
	statuses: 'status',
	types: 'type',
};

/**
	@param {*} variant - PropTypes.oneOf(['types', 'statuses', 'priorities']).isRequired
	@param {*} task - optional, task object with corresponding enum property
	@returns enum item if task is provided; dummy enum item if task is provided but not found
 */
export default function useEnum(variant, task = undefined) {
	const enumMap = useSelector((state) => state.enums.map);
	const field = FIELD_MAP[variant];
	const item = enumMap[task[field]];

	// if we have a task and we found an item, return the item
	if (item) return item;

	// if we have a task and we did not find an item, return a dummy
	return { name: field, description: `${task[field]} unknown`, icon: 'BrokenImageIcon' };
}
