import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { Avatar, useTheme } from '@material-ui/core';

import EnumIcon from './EnumIcon';
import tasksService from '../../services/tasksService';

export default function EnumAvatar({ fontSize, task }) {
	const theme = useTheme();
	const style = useMemo(() => ({
		backgroundColor: (task.parent ? undefined : theme.palette.primary.main),
		width: (fontSize === 'small' ? '1.5em' : undefined),
		height: (fontSize === 'small' ? '1.5em' : undefined),
	}), [fontSize, task, theme]);

	return (
		<Avatar style={style}>
			<EnumIcon task={task} variant="types" fontSize={fontSize} />
		</Avatar>
	);
}

EnumAvatar.propTypes = {
	fontSize: PropTypes.oneOf(['inherit', 'small', 'medium', 'large']),
	task: tasksService.propTypes.isRequired,
};

EnumAvatar.defaultProps = {
	fontSize: 'medium',
};
