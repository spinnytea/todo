import React, { useCallback, useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import styled from 'styled-components';

import EnumEditPanel from './EnumEditPanel';
import useEnums from './useEnums';
import { Alert, IconButton, RefreshIcon } from '../mui';
import * as enumActions from '../../redux/actions/enumActions';
import enumsService from '../../services/enumsService';

const StyledContainer = styled.div`
	margin: ${(props) => props.theme.spacing(1)}px;
`;

const StyledSection = styled.section`
	display: grid;
	gap: ${(props) => props.theme.spacing(1)}px;
	grid-template-columns: repeat(3, 1fr);
`;

export default function EnumList({ variant }) {
	const [listError, setListError] = useState();
	const [createError, setCreateError] = useState();
	const [saveError, setSaveError] = useState();

	/* List */

	const orig = useEnums(variant);
	const [list, setList] = useState(orig);
	useEffect(() => { setList(orig); }, [orig]);

	const [isRefreshing, setRefreshing] = useState(false);
	const doRefresh = useCallback(() => {
		setRefreshing(true);
		setListError(undefined);

		enumActions.refresh(variant)
			.catch(({ message }) => { setListError(message); })
			.finally(() => setRefreshing(false));
	}, [variant]);

	const updateItem = useCallback((item) => {
		setList(list.map((i) => (i.id === item.id ? item : i)));
	}, [list]);

	const saveItem = useCallback((event, item) => {
		setRefreshing(true);
		setSaveError(undefined);

		enumsService.save(variant, item)
			.then(() => enumActions.relocal(variant, list))
			.catch(({ message }) => { setSaveError(message); })
			.finally(() => setRefreshing(false));
	}, [list, variant]);

	/* New */

	const NEW_ITEM = useMemo(() => ({
		...(variant === 'statuses' && { category: '0' }),
		description: '',
		icon: 'RefreshIcon',
		id: 'New!',
		name: '',
		order: 0,
	}), [variant]);
	const [newItem, updateNewItem] = useState({ ...NEW_ITEM });

	const saveNewItem = useCallback(() => {
		if (isRefreshing) return;
		setRefreshing(true);
		setCreateError(undefined);

		enumsService.create(variant, newItem)
			.then(() => { updateNewItem({ ...NEW_ITEM }); })
			.catch(({ message }) => { setCreateError(message); })
			.finally(() => doRefresh());
	}, [isRefreshing, doRefresh, NEW_ITEM, newItem, variant]);

	return (
		<StyledContainer>
			<Typography variant="h3" component="h2">
				Enum:&nbsp;
				{variant}&nbsp;
				<IconButton actionLabel="Refresh" Icon={RefreshIcon} fontSize="large" onClick={doRefresh} disabled={isRefreshing} />
			</Typography>
			<Alert message={listError} />
			<Alert message={saveError} />
			<Alert message={createError} />
			<StyledSection>
				{list.map((item) => (
					<EnumEditPanel key={item.id} item={item} variant={variant} onChange={updateItem} onSubmit={saveItem} disabled={isRefreshing} />
				))}
				<EnumEditPanel item={newItem} variant={variant} onChange={updateNewItem} onSubmit={saveNewItem} disabled={isRefreshing} />
			</StyledSection>
		</StyledContainer>
	);
}

EnumList.propTypes = {
	variant: PropTypes.oneOf(['types', 'statuses', 'priorities']).isRequired,
};
