import useEnum from './useEnum';
import enumsService from '../../services/enumsService';

export default function useStatusCategory(task) {
	const status = useEnum('statuses', task);
	const statusCategory = enumsService.statusCategories.find(({ id }) => id === status.category);
	return statusCategory;
}
