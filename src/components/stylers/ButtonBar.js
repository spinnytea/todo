import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledDiv = styled.div`
	display: flex;
	align-items: center;
	justify-content: left;

	margin: ${(props) => props.theme.spacing(2, 0)};

	> * {
		margin: ${(props) => props.theme.spacing(0, 2, 0, 0)};
	}

	.MuiSelect-root,
	.MuiInput-root {
		min-width: 20ch;
	}
`;

export default function ButtonBar({ className, children }) {
	return (
		<StyledDiv className={className}>
			{children}
		</StyledDiv>
	);
}

ButtonBar.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node,
	]).isRequired,
	className: PropTypes.string,
};

ButtonBar.defaultProps = {
	className: undefined,
};
