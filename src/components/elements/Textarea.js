import React, { useCallback, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { FormControl, InputLabel, Input as MuiInput } from '@material-ui/core';
import { debounce } from 'lodash';

import utils from '../../utils';

export default function Textarea(props) {
	const {
		className,
		debounced,
		disabled,
		display = utils.nameToDisplay(props.name), // eslint-disable-line react/destructuring-assignment
		id = props.name, // eslint-disable-line react/destructuring-assignment
		name,
		onSubmit,
		rows,
		value,
		valueChange,
	} = props;

	const [myValue, setMyValue] = useState(value);
	const debounceValueChange = useCallback((debounced // eslint-disable-line react-hooks/exhaustive-deps
		? debounce(valueChange, (debounced === 'long' ? utils.LONG_DELAY : utils.SHORT_DELAY))
		: valueChange
	), [valueChange, debounced]);
	useEffect(() => {
		setMyValue((v) => {
			if (v !== value) return value;
			return v;
		});
	}, [value, setMyValue]);

	// textareas can have multi-lines, so the enter key does not trigger submit on the form
	// on ctrl+enter, trigger the form submit
	const handleKeyDown = useCallback((event) => {
		const key = event.which || event.keyCode;
		if (onSubmit && key === 13 && (event.shiftKey || event.ctrlKey || event.metaKey)) {
			event.preventDefault();
			onSubmit(new Event('submit', { bubbles: true, cancelable: true }));
		}
	}, [onSubmit]);

	const onChange = useCallback((event) => {
		const newValue = event.target.value;
		setMyValue(newValue);

		if (valueChange) {
			debounceValueChange([name, newValue, event]);
		}
	}, [valueChange, name, debounceValueChange, setMyValue]);

	return (
		<FormControl
			className={className}
			disabled={disabled}
			fullWidth
		>
			<InputLabel htmlFor={id} shrink>{display} ({myValue?.length || 0})</InputLabel>
			<MuiInput
				id={id}
				name={name}
				multiline
				rows={rows}
				value={myValue}
				onChange={onChange}
				onKeyDown={handleKeyDown}
			/>
		</FormControl>
	);
}

Textarea.propTypes = {
	// i/o
	value: PropTypes.string,
	valueChange: PropTypes.func,
	onSubmit: PropTypes.func,

	// optional
	className: PropTypes.string,
	debounced: PropTypes.oneOf(['long', 'short']),
	disabled: PropTypes.bool,
	display: PropTypes.string,
	id: PropTypes.string,
	name: PropTypes.string,
	rows: PropTypes.oneOfType([
		PropTypes.number,
		PropTypes.string,
	]),
};

Textarea.defaultProps = {
	className: undefined,
	debounced: undefined,
	disabled: false,
	display: undefined,
	id: undefined,
	name: undefined,
	onSubmit: null,
	rows: 3,
	value: '',
	valueChange: null,
};
