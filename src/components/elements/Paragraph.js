import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import styled from 'styled-components';

const StyledTypography = styled(Typography)`
	margin: ${(props) => props.theme.spacing((props.$mt ? 2 : 0), 0, 2, 0)};

	&:last-child {
		margin-bottom: 0;
	}
`;

const StyledRemovedTypography = styled(StyledTypography)`
	opacity: ${(props) => props.theme.palette.action.disabledOpacity};
	font-style: italic;
`;

export default function Paragraph(props) {
	const {
		children,
		mt,
		removed,
		...rest
	} = props;

	if (removed) {
		return (
			<StyledRemovedTypography component="div" {...rest}> {/* eslint-disable-line react/jsx-props-no-spreading */}
				{children}
			</StyledRemovedTypography>
		);
	}

	return (
		<StyledTypography component="div" $mt={mt} {...rest}> {/* eslint-disable-line react/jsx-props-no-spreading */}
			{children}
		</StyledTypography>
	);
}

Paragraph.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node,
	]),
	mt: PropTypes.bool,
	removed: PropTypes.bool,
	variant: PropTypes.oneOf(['body1', 'body2']),
};

Paragraph.defaultProps = {
	children: undefined,
	mt: true,
	removed: false,
	variant: 'body1',
};
