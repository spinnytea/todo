import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { FormControlLabel, Switch as MuiSwitch } from '@material-ui/core';

import utils from '../../utils';

export default function Checkbox(props) {
	const {
		className,
		disabled,
		display = utils.nameToDisplay(props.name), // eslint-disable-line react/destructuring-assignment
		id = props.name, // eslint-disable-line react/destructuring-assignment
		name,
		value,
		valueChange,
	} = props;

	const onChange = useCallback((event) => {
		if (valueChange) {
			valueChange([name, event.target.checked, event]);
		}
	}, [valueChange, name]);

	return (
		<FormControlLabel
			className={className}
			disabled={disabled}
			control={(
				<MuiSwitch
					checked={value}
					color="primary"
					id={id}
					name={name}
					onChange={onChange}
				/>
			)}
			label={display}
		/>
	);
}

Checkbox.propTypes = {
	// i/o
	// value is based on input[type]
	value: PropTypes.bool,
	valueChange: PropTypes.func,

	// optional
	className: PropTypes.string,
	disabled: PropTypes.bool,
	display: PropTypes.string,
	id: PropTypes.string,
	name: PropTypes.string,
};

Checkbox.defaultProps = {
	className: undefined,
	disabled: false,
	display: undefined,
	id: undefined,
	name: undefined,
	value: false,
	valueChange: null,
};
