import React, { useCallback, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { FormControl, InputLabel, Input as MuiInput } from '@material-ui/core';
import { debounce } from 'lodash';

import utils from '../../utils';

export default function Input(props) {
	const {
		autoFocus,
		className,
		debounced,
		disabled,
		display = utils.nameToDisplay(props.name), // eslint-disable-line react/destructuring-assignment
		fullWidth,
		id = props.name, // eslint-disable-line react/destructuring-assignment
		name,
		required,
		type,
		value,
		valueChange,
	} = props;

	const [myValue, setMyValue] = useState(value);
	const debounceValueChange = useCallback((debounced // eslint-disable-line react-hooks/exhaustive-deps
		? debounce(valueChange, (debounced === 'long' ? utils.LONG_DELAY : utils.SHORT_DELAY))
		: valueChange
	), [valueChange, debounced]);
	useEffect(() => {
		setMyValue((v) => {
			if (v !== value) return value;
			return v;
		});
	}, [value, setMyValue]);

	const onChange = useCallback((event) => {
		let newValue = event.target.value;
		setMyValue(newValue);

		if (valueChange) {
			if (type === 'number') {
				newValue = parseInt(newValue, 10);
			}

			debounceValueChange([name, newValue, event]);
		}
	}, [valueChange, debounceValueChange, name, type]);

	return (
		<FormControl
			className={className}
			disabled={disabled}
			fullWidth={fullWidth}
			required={required}
		>
			<InputLabel htmlFor={id} shrink>{display}</InputLabel>
			<MuiInput
				id={id}
				name={name}
				type={type}
				value={myValue}
				onChange={onChange}
				autoFocus={autoFocus}
			/>
		</FormControl>
	);
}

Input.propTypes = {
	// i/o
	// value is based on input[type]
	value: PropTypes.any, // eslint-disable-line react/forbid-prop-types
	valueChange: PropTypes.func,

	// optional
	autoFocus: PropTypes.bool,
	className: PropTypes.string,
	debounced: PropTypes.oneOf(['long', 'short']),
	disabled: PropTypes.bool,
	display: PropTypes.string,
	fullWidth: PropTypes.bool,
	id: PropTypes.string,
	name: PropTypes.string,
	required: PropTypes.bool,
	type: PropTypes.string,
};

Input.defaultProps = {
	autoFocus: false,
	className: undefined,
	debounced: undefined,
	disabled: false,
	display: undefined,
	fullWidth: true,
	id: undefined,
	name: undefined,
	required: false,
	type: 'text',
	value: '',
	valueChange: null,
};
