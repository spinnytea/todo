import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { Chip, TextField } from '@material-ui/core';
import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import styled from 'styled-components';
import { sortBy } from 'lodash';

import Paragraph from './Paragraph';
import utils from '../../utils';

const filter = createFilterOptions();
function allowNew(options, params) {
	const filtered = filter(options, params);

	// Suggest the creation of a new value
	if (filtered.length === 0 && params.inputValue !== '') {
		filtered.push({
			inputValue: params.inputValue,
			display: `Add "${params.inputValue}"`,
		});
	}

	return filtered;
}

const StyledContainer = styled.div`
	display: flex;
	gap: ${(props) => props.theme.spacing(1)}px;
	flex-wrap: wrap;
`;

export default function Tags(props) {
	const {
		className,
		display = utils.nameToDisplay(props.name), // eslint-disable-line react/destructuring-assignment
		id = props.name, // eslint-disable-line react/destructuring-assignment
		name,
		onSelect,
		readonly,
		value,
		valueChange,
	} = props;

	const options = props.options // eslint-disable-line react/destructuring-assignment
		.map((o) => ({ inputValue: o, display: o }));

	const [autocompleteValue, setAutocompleteValue] = useState(null);

	const handleDelete = useCallback((event, idx) => {
		if (valueChange) {
			const update = value.slice(0);
			update.splice(idx, 1);
			valueChange([name, update, event]);
		}
	}, [valueChange, name, value]);

	const handleSelect = useCallback((event, { inputValue }) => {
		setAutocompleteValue(null); // BUG clear input value immediately
		if (valueChange && value.indexOf(inputValue) === -1) {
			const update = sortBy(value.concat([inputValue]), (i) => i);
			valueChange([name, update, event]);
		}
	}, [valueChange, name, value, setAutocompleteValue]);

	return (
		<StyledContainer className={className}>
			{readonly ? null : (
				<Autocomplete
					autoHighlight
					clearOnBlur
					clearOnEscape
					filterOptions={allowNew}
					fullWidth
					getOptionLabel={(option) => option.display}
					id={id}
					options={options}
					value={autocompleteValue}
					onChange={handleSelect}
					renderInput={(params) => (
						<TextField
							{...params} // eslint-disable-line react/jsx-props-no-spreading
							label={display}
							name={name}
						/>
					)}
					renderOption={(option) => option.display}
				/>
			)}

			{value.map((label, idx) => (
				<Chip
					key={label}
					label={label}
					onClick={onSelect ? (event) => onSelect(event, label) : undefined}
					onDelete={readonly ? undefined : (event) => handleDelete(event, idx)}
					size="small"
				/>
			))}

			{value.length ? null : <Paragraph removed>none</Paragraph>}
		</StyledContainer>
	);
}

Tags.propTypes = {
	// i/o
	value: PropTypes.arrayOf(PropTypes.string),
	valueChange: PropTypes.func,
	onSelect: PropTypes.func,

	// optional
	className: PropTypes.string,
	display: PropTypes.string,
	id: PropTypes.string,
	name: PropTypes.string,
	options: PropTypes.arrayOf(PropTypes.string),
	readonly: PropTypes.bool,
};

Tags.defaultProps = {
	className: undefined,
	display: undefined,
	id: undefined,
	name: undefined,
	onSelect: undefined,
	options: [],
	readonly: false,
	value: [],
	valueChange: null,
};
