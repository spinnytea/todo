import React from 'react';
import PropTypes from 'prop-types';
import ReactMarkdown from 'react-markdown';
import styled from 'styled-components';

const StyledContainer = styled.div`
	> p {
		margin: 0;
	}
`;

export default function Markdown(props) {
	const {
		children,
		className,
	} = props;

	return (
		<StyledContainer className={className}>
			<ReactMarkdown>
				{children}
			</ReactMarkdown>
		</StyledContainer>
	);
}

Markdown.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node,
	]),
	className: PropTypes.string,
};

Markdown.defaultProps = {
	children: undefined,
	className: undefined,
};
