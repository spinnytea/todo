import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { FormControl, InputAdornment, InputLabel, Select as MuiSelect } from '@material-ui/core';

import utils from '../../utils';

export default function Select(props) {
	const {
		children: options,
		className,
		disabled,
		display = utils.nameToDisplay(props.name), // eslint-disable-line react/destructuring-assignment
		id = props.name, // eslint-disable-line react/destructuring-assignment
		fullWidth,
		name,
		required,
		startAdornment,
		value,
		valueChange,
	} = props;

	const onChange = useCallback((event) => {
		if (valueChange) {
			valueChange([name, event.target.value, event]);
		}
	}, [valueChange, name]);

	return (
		<FormControl
			className={className}
			disabled={disabled}
			fullWidth={fullWidth}
			required={required}
		>
			<InputLabel htmlFor={id} shrink>{display}</InputLabel>
			<MuiSelect
				native
				value={value}
				onChange={onChange}
				inputProps={{
					name,
					id,
				}}
				startAdornment={startAdornment ? (
					<InputAdornment position="start">
						{startAdornment}
					</InputAdornment>
				) : null}
			>
				{options}
			</MuiSelect>
		</FormControl>
	);
}

Select.propTypes = {
	// i/o
	value: PropTypes.any, // eslint-disable-line react/forbid-prop-types
	valueChange: PropTypes.func,
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node,
	]).isRequired,

	// optional
	className: PropTypes.string,
	disabled: PropTypes.bool,
	display: PropTypes.string,
	fullWidth: PropTypes.bool,
	id: PropTypes.string,
	name: PropTypes.string,
	required: PropTypes.bool,
	startAdornment: PropTypes.node,
};

Select.defaultProps = {
	className: undefined,
	disabled: false,
	display: undefined,
	fullWidth: true,
	id: undefined,
	name: undefined,
	required: false,
	startAdornment: undefined,
	value: '',
	valueChange: null,
};
