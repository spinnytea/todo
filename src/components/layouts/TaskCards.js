import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { List, ListItem } from '@material-ui/core';
import styled from 'styled-components';

import TaskReadonlyPanel from '../TaskReadonlyPanel';
import tasksService from '../../services/tasksService';

const StyledROTasks = styled(List)`
	display: grid;
	grid-template-columns: repeat(4, 1fr);
`;

const StyledROTask = styled(ListItem)`
	display: block;
	padding: ${(props) => props.theme.spacing(1)}px;
	margin: 0;
`;

export default function TaskCards({ tasks, onSelect }) {
	return (
		<StyledROTasks>
			{tasks.map((task) => (
				<TaskCard key={task.id} task={task} onSelect={onSelect} />
			))}
		</StyledROTasks>
	);
}

function TaskCard({ task, onSelect }) {
	const handleSelect = useCallback((event) => {
		onSelect(event, task);
	}, [task, onSelect]);

	return (
		<StyledROTask key={task.id} component="li" button onClick={handleSelect}>
			<TaskReadonlyPanel task={task} />
		</StyledROTask>
	);
}

TaskCards.propTypes = {
	tasks: PropTypes.arrayOf(tasksService.propTypes).isRequired,
	onSelect: PropTypes.func.isRequired,
};

TaskCard.propTypes = {
	task: tasksService.propTypes.isRequired,
	onSelect: PropTypes.func.isRequired,
};
