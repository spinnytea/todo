import React, { useCallback, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { capitalize, orderBy } from 'lodash';

import TaskCards from './TaskCards';
import { ButtonBar } from '../stylers';
import { Select } from '../elements';
import enumsService from '../../services/enumsService';
import tasksService from '../../services/tasksService';

const SORT_FIELDS = ['default', 'priority', 'status', 'created', 'updated'];

export default function TaskCardsLayout({ tasks: tasksIn, onSelect }) {
	const enumMap = useSelector((state) => state.enums.map);
	const [{ sortBy = 'default' }, setSort] = useState({});
	const tasks = useMemo(() => {
		switch (sortBy) {
		case 'default':
			return tasksIn;
		case 'priority':
		case 'status':
		case 'type':
			return orderBy(tasksIn, enumsService.orderBy(enumMap, sortBy), 'desc');
		default:
			return orderBy(tasksIn, sortBy, 'desc');
		}
	}, [tasksIn, sortBy, enumMap]);

	const handleSortChange = useCallback(([name, newValue]) => {
		setSort((s) => ({ ...s, [name]: newValue }));
	}, [setSort]);

	return (
		<div>
			<ButtonBar>
				<Select
					id="options-sort"
					name="sortBy"
					value={sortBy}
					valueChange={handleSortChange}
					fullWidth={false}
				>
					{SORT_FIELDS.map((field) => (
						<option key={field} value={field} aria-label={field}>{capitalize(field)}</option>
					))}
				</Select>
			</ButtonBar>
			<TaskCards
				tasks={tasks}
				onSelect={onSelect}
			/>
		</div>
	);
}

TaskCardsLayout.propTypes = {
	tasks: PropTypes.arrayOf(tasksService.propTypes).isRequired,
	onSelect: PropTypes.func.isRequired,
};
