import React, { useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import { List, ListItem, Typography } from '@material-ui/core';
import styled from 'styled-components';
import _ from 'lodash';

import { EnumAvatar, EnumIcon, useStatusCategory } from '../enums';
import tasksService from '../../services/tasksService';

const StyledList = styled(List)`
	margin-left: ${(props) => (props.$depth === 0 ? '0' : '2em')};
	padding: 0;
`;

const StyledListItem = styled(ListItem)`
	padding: ${(props) => props.theme.spacing(1, 1)};
	> * + * {
		margin-left: ${(props) => props.theme.spacing(1)}px;
	}
`;

const StyledName = styled(Typography)`
	display: flex;
	align-items: center;
	justify-content: flex-start;
	color: ${(props) => props.$statusCategory?.accentColor};
`;

export default function TaskTreeLayout({ tasks, onSelect }) {
	const subtree = useMemo(() => {
		const nodeMap = _.cloneDeep(tasks).reduce((ret, task) => { ret[task.id] = { task }; return ret; }, {});
		const rootNodes = [];

		// make sure this tree maintains the original order of the tasks
		tasks.forEach(({ id, parent }) => {
			// if there is no parent, add the task to the root nodes
			// if there is a parent, put the task under the parent group
			const parentNode = nodeMap[parent?.id];
			const currNode = nodeMap[id];
			if (parentNode) {
				if (!parentNode.subtree) parentNode.subtree = [];
				parentNode.subtree.push(currNode);
			}
			else {
				rootNodes.push(currNode);
			}
		});

		return rootNodes;
	}, [tasks]);

	return (
		<TaskList subtree={subtree} depth={0} onSelect={onSelect} />
	);
}

function TaskList({ subtree, depth, onSelect }) {
	return (
		<StyledList $depth={depth}>
			{subtree.map((node) => (
				<React.Fragment key={node.task.id}>
					<TaskLineItem task={node.task} onSelect={onSelect} />
					{node.subtree?.length > 0 && (
						<TaskList subtree={node.subtree} depth={depth + 1} onSelect={onSelect} />
					)}
				</React.Fragment>
			))}
		</StyledList>
	);
}

function TaskLineItem({ task, onSelect }) {
	const statusCategory = useStatusCategory(task);

	const handleSelect = useCallback((event) => {
		onSelect(event, task);
	}, [task, onSelect]);

	return (
		<StyledListItem key={task.id} button onClick={handleSelect}>
			<EnumAvatar task={task} fontSize="small" />
			<StyledName variant="body1" $statusCategory={statusCategory} component="p">
				<EnumIcon task={task} variant="statuses" />
				<EnumIcon task={task} variant="priorities" />
			</StyledName>
			<StyledName variant="body1" $statusCategory={statusCategory} component="p">
				{task.name} (ID: {task.id})
			</StyledName>
		</StyledListItem>
	);
}

TaskTreeLayout.propTypes = {
	tasks: PropTypes.arrayOf(tasksService.propTypes.isRequired).isRequired,
	onSelect: PropTypes.func.isRequired,
};

// subtree is recursive, so we need to get a little tricky
const subtreeShape = PropTypes.shape({
	task: tasksService.propTypes.isRequired, // eslint-disable-line react/forbid-foreign-prop-types
});
const subtreeArray = PropTypes.arrayOf(subtreeShape);
subtreeShape.subtree = subtreeArray;

TaskList.propTypes = {
	subtree: subtreeArray.isRequired,
	depth: PropTypes.number.isRequired,
	onSelect: PropTypes.func.isRequired,
};

TaskLineItem.propTypes = {
	task: tasksService.propTypes.isRequired,
	onSelect: PropTypes.func.isRequired,
};
