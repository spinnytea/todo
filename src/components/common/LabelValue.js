import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';

function cleanValue(value) {
	switch (value) {
	case true: return 'True';
	case false: return 'False';
	default: return value;
	}
}

export default function LabelValue({ label, value }) {
	return (
		<>
			<Typography
				className="label"
				component="label"
				variant="body1"
				htmlFor={label}
			>
				{label}:
			</Typography>
			<Typography
				className="value"
				component="output"
				variant="body2"
				color="textSecondary"
				name={label}
			>
				{cleanValue(value)}
			</Typography>
		</>
	);
}

LabelValue.propTypes = {
	label: PropTypes.string.isRequired,
	value: PropTypes.any, // eslint-disable-line react/forbid-prop-types
};

LabelValue.defaultProps = {
	value: '—',
};
