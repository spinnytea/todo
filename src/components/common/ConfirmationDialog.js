import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import styled from 'styled-components';
import { isString } from 'lodash';

import { Button, CloseIcon, Dialog, IconButton } from '../mui';
import { Paragraph } from '../elements';

const StyledHeader = styled.header`
	display: flex;
	align-items: center;
	justify-content: space-between;

	h2 {
		line-height: 1em;
		margin: ${(props) => props.theme.spacing(2)}px;
	}

	[aria-label="Close"] {
		padding: ${(props) => props.theme.spacing(1)}px;
		margin: ${(props) => props.theme.spacing(2) - props.theme.spacing(1)}px;
	}
`;

const StyledSection = styled.section`
	margin: ${(props) => props.theme.spacing(0, 2)};
`;

const StyledFooter = styled.footer`
	display: flex;
	align-items: center;
	justify-content: flex-end;

	margin: ${(props) => props.theme.spacing(2, 2, 2, 2)};
	gap: ${(props) => props.theme.spacing(1)}px;
`;

export default function ConfirmationDialog({ actionColor, actionLabel, message: messageIn, open, title, onCancel, onClose: onCloseIn, onConfirm }) {
	const onClose = onCloseIn || onCancel;

	const message = useMemo(() => {
		if (isString(messageIn)) {
			return messageIn.split('\\n');
		}
		return message;
	}, [messageIn]);

	return (
		<Dialog
			open={open}
			maxWidth="xs"
			onMetaClose={onClose}
		>
			<StyledHeader>
				<Typography component="h2" variant="h6">
					{title}
				</Typography>
				<IconButton actionLabel="Close" Icon={CloseIcon} onClick={onClose} />
			</StyledHeader>
			<StyledSection>
				{message.map((m) => (
					<Paragraph key={m} mt={false}>{m}</Paragraph>
				))}
			</StyledSection>
			<StyledFooter>
				<Button type="submit" variant="contained" actionColor={actionColor} actionLabel={actionLabel} onClick={onConfirm} />
				<Button type="button" variant="outlined" actionColor={actionColor} actionLabel="Cancel" onClick={onCancel} />
			</StyledFooter>
		</Dialog>
	);
}

ConfirmationDialog.propTypes = {
	actionColor: PropTypes.oneOf(['primary', 'info', 'success', 'warning', 'error']),
	actionLabel: PropTypes.string.isRequired,
	message: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.string),
		PropTypes.string,
	]).isRequired,
	open: PropTypes.bool,
	title: PropTypes.string.isRequired,

	onCancel: PropTypes.func.isRequired,
	onClose: PropTypes.func,
	onConfirm: PropTypes.func.isRequired,
};

ConfirmationDialog.defaultProps = {
	actionColor: 'primary',
	open: false,
	onClose: null,
};
