import React from 'react';
import PropTypes from 'prop-types';

import SuccessFilled from '@material-ui/icons/CheckCircle';
import InfoFilled from '@material-ui/icons/InfoRounded';
import WarningFilled from '@material-ui/icons/WarningRounded';
import ErrorFilled from '@material-ui/icons/Cancel';

import SuccessOutlined from '@material-ui/icons/CheckCircleOutlined';
import InfoOutlined from '@material-ui/icons/InfoOutlined';
import WarningOutlined from '@material-ui/icons/ReportProblemOutlined';
import ErrorOutlined from '@material-ui/icons/CancelOutlined';

/**
	a mapping of default icons by severity and variant
*/
export default function StatusIcon({ fontSize, severity, variant, ...rest }) {
	let Icon;

	switch (variant) {
	case 'filled':
		switch (severity) {
		case 'success': Icon = SuccessFilled; break;
		case 'info': Icon = InfoFilled; break;
		case 'warning': Icon = WarningFilled; break;
		case 'error': Icon = ErrorFilled; break;
		default: Icon = null; break;
		}
		break;
	case 'outlined':
		switch (severity) {
		case 'success': Icon = SuccessOutlined; break;
		case 'info': Icon = InfoOutlined; break;
		case 'warning': Icon = WarningOutlined; break;
		case 'error': Icon = ErrorOutlined; break;
		default: Icon = null; break;
		}
		break;
	default: Icon = null; break;
	}

	if (Icon) {
		return <Icon fontSize={fontSize} {...rest} />; // eslint-disable-line react/jsx-props-no-spreading
	}

	return null;
}

StatusIcon.propTypes = {
	fontSize: PropTypes.oneOf(['inherit', 'small', 'medium', 'large']),
	severity: PropTypes.oneOf(['info', 'success', 'warning', 'error']).isRequired,
	variant: PropTypes.oneOf(['filled', 'outlined']),
};

StatusIcon.defaultProps = {
	fontSize: undefined,
	variant: 'filled',
};
