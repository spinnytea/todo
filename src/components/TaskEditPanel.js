import React, { useCallback, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Card, CardActions, CardContent, CardHeader, Typography } from '@material-ui/core';
import styled from 'styled-components';

import TaskAutocomplete from './TaskAutocomplete';
import useTags from './useTags';
import { ConfirmationDialog } from './common';
import { EnumAvatar, EnumIcon, useEnums } from './enums';
import { Alert, Button, CloseIcon, DeleteIcon, Divider, ExpandLessIcon, ExpandMoreIcon, IconButton } from './mui';
import { Input, Select, Tags, Textarea } from './elements';
import tasksService from '../services/tasksService';
import utils from '../utils';

const StyledGrid = styled.div`
	margin-top: ${(props) => props.theme.spacing(2)}px;

	display: grid;
	grid-template-columns: 1fr 1fr;
	gap: ${(props) => props.theme.spacing(2)}px;

	> div {
		display: flex;
		flex-direction: column;
		gap: ${(props) => props.theme.spacing(2)}px;
	}
`;

const StyledAlert = styled(Alert)`
	margin-top: ${(props) => props.theme.spacing(2)}px;
`;

const StyledDeleteButton = styled(IconButton)`
	&:not(:hover) {
		svg {
			color: ${(props) => props.theme.palette.action.active};
		}
	}
`;

/**
	You always need a fallback to create/edit a task in one shot.
	The goal is to not use this at all.
*/
export default function TaskEditPanel({ disabled, errorMessage, forceOpen, task, onChange, onClose, onDelete, onSubmit, variant }) {
	const types = useEnums('types');
	const statuses = useEnums('statuses');
	const priorities = useEnums('priorities');
	const tags = useTags();

	const [collapsed, setCollapsed] = useState(variant !== 'create');
	useEffect(() => { if (forceOpen) { setCollapsed(false); } }, [forceOpen]);
	const toggleCollapsed = useCallback(() => { setCollapsed(!collapsed); }, [collapsed]);

	disabled = disabled || !tasksService.isTaskValid(task);

	const handleChange = useCallback(([name, value]) => {
		onChange({
			...task,
			[name]: value,
		});
	}, [onChange, task]);

	const handleSubmit = useCallback((event) => {
		event.preventDefault();
		event.stopPropagation();
		if (disabled) return;
		if (onSubmit) onSubmit(event, task);
	}, [onSubmit, task, disabled]);

	const [confirmDelete, setConfirmDelete] = useState(false);
	const openConfirmDelete = useCallback(() => { setConfirmDelete(true); }, [setConfirmDelete]);
	const closeConfirmDelete = useCallback(() => { setConfirmDelete(false); }, [setConfirmDelete]);
	const handleOnDelete = useCallback(() => {
		onDelete();
		closeConfirmDelete();
	}, [onDelete, closeConfirmDelete]);

	const cardHeaderAction = (
		<>
			{variant === 'edit' && !collapsed && (
				<>
					<StyledDeleteButton
						actionLabel="Delete"
						actionColor="error"
						Icon={DeleteIcon}
						onClick={openConfirmDelete}
					/>
					<ConfirmationDialog
						open={confirmDelete}
						title={`Delete Task ID: ${task.id}`}
						message="This action is permanent.\nAre you sure you want to delete this task forever?"
						actionColor="error"
						actionLabel="Delete"
						onConfirm={handleOnDelete}
						onCancel={closeConfirmDelete}
					/>
				</>
			)}
			{variant === 'edit' && (
				<IconButton
					actionLabel={collapsed ? 'Expand' : 'Collapse'}
					onClick={toggleCollapsed}
					Icon={collapsed ? ExpandMoreIcon : ExpandLessIcon}
				/>
			)}
			{variant !== 'collapsed' && (
				<IconButton
					actionLabel="Close"
					Icon={CloseIcon}
					onClick={onClose}
				/>
			)}
		</>
	);

	return (
		<Card elevation={2} component="form" onSubmit={handleSubmit}>
			{collapsed ? (
				<CardHeader
					title={task.name}
					subheader={`Task ID: ${task.id}`}
					avatar={<EnumAvatar task={task} />}
					action={cardHeaderAction}
				/>
			) : (
				<CardHeader
					subheader={`Task ID: ${task.id}`}
					action={cardHeaderAction}
				/>
			)}
			{!collapsed && (
				<CardContent>
					<StyledGrid>
						<div>
							<Input
								id={`task-${task.id}-name`}
								name="name"
								required
								debounced="long"
								autoFocus
								value={task.name}
								valueChange={handleChange}
							/>
							<Textarea
								id={`task-${task.id}-description`}
								name="description"
								debounced="long"
								value={task.description}
								valueChange={handleChange}
								onSubmit={handleSubmit}
							/>
							<TaskAutocomplete
								id={`task-${task.id}-parent`}
								name="parent"
								value={task.parent}
								valueChange={handleChange}
							/>
							<Tags
								id={`task-${task.id}-tags`}
								name="tags"
								options={tags}
								value={task.tags}
								valueChange={handleChange}
							/>
						</div>
						<div>
							<Select
								id={`task-${task.id}-type`}
								name="type"
								required
								value={task.type}
								valueChange={handleChange}
								startAdornment={<EnumIcon task={task} variant="types" />}
							>
								{types.length === 0 && (<option value="" aria-label="">None</option>)}
								{types.map(({ id, name }) => (
									<option key={id} value={id} aria-label={name}>{name}</option>
								))}
							</Select>
							<Select
								id={`task-${task.id}-status`}
								name="status"
								required
								value={task.status}
								valueChange={handleChange}
								startAdornment={<EnumIcon task={task} variant="statuses" />}
							>
								{statuses.length === 0 && (<option value="" aria-label="">None</option>)}
								{statuses.map(({ id, name }) => (
									<option key={id} value={id} aria-label={name}>{name}</option>
								))}
							</Select>
							<Select
								id={`task-${task.id}-priority`}
								name="priority"
								required
								value={task.priority}
								valueChange={handleChange}
								startAdornment={<EnumIcon task={task} variant="priorities" />}
							>
								{priorities.length === 0 && (<option value="" aria-label="">None</option>)}
								{priorities.map(({ id, name }) => (
									<option key={id} value={id} aria-label={name}>{name}</option>
								))}
							</Select>
						</div>
					</StyledGrid>
					<StyledAlert message={errorMessage} />
				</CardContent>
			)}
			{variant === 'create' && (
				<CardActions style={{ justifyContent: 'flex-end' }}>
					<Button type="submit" actionLabel="Save" onClick={handleSubmit} disabled={disabled} />
				</CardActions>
			)}
			{variant === 'edit' && !collapsed && (
				<CardActions style={{ justifyContent: 'flex-end' }}>
					<Typography variant="body1" color="textSecondary" component="p">
						Updated {utils.time.fromNow(task.updated)}
					</Typography>
					<Divider orientation="vertical" />
					<Typography variant="body1" color="textSecondary" component="p">
						Created {utils.time.format(task.created)}
					</Typography>
				</CardActions>
			)}
		</Card>
	);
}

TaskEditPanel.propTypes = {
	disabled: PropTypes.bool,
	errorMessage: PropTypes.string,
	forceOpen: PropTypes.bool,
	onClose: PropTypes.func,
	onChange: PropTypes.func,
	onDelete: PropTypes.func,
	onSubmit: PropTypes.func,
	task: tasksService.propTypes.isRequired,
	variant: PropTypes.oneOf(['create', 'edit', 'collapsed']).isRequired,
};

TaskEditPanel.defaultProps = {
	disabled: false,
	errorMessage: undefined,
	forceOpen: true,
	onChange: null,
	onClose: null,
	onDelete: null,
	onSubmit: null,
};
