import { useCallback } from 'react';
import { useNavigate, useLocation } from 'react-router';
import queryString from 'query-string';
import queryStringParse from './queryStringParse';

export default function useQueryParams() {
	const navigate = useNavigate();
	const { search } = useLocation(); // need to call useLocation otherwise ui won't refresh when this changes
	const queryParams = queryStringParse(search);

	const updateQueryParams = useCallback((updates) => {
		if (updates.closed === false || updates.closed === 'false') updates.closed = undefined;
		if (updates.inprogress === false || updates.inprogress === 'false') updates.inprogress = undefined;
		if (updates.query === '') updates.query = undefined;

		const newSearch = queryString.stringify({ ...queryParams, ...updates });
		if (search !== newSearch) {
			navigate({
				search: newSearch,
			});
		}
	}, [navigate, search, queryParams]);

	return [queryParams, updateQueryParams];
}
