import moment from 'moment';
import { groupBy, keyBy, orderBy, sortBy, startCase } from 'lodash';

const utils = {
	SHORT_DELAY: 400,
	LONG_DELAY: 1000,

	nameToDisplay: (name) => startCase(name),

	time: {
		format: (date) => moment(date).format('MMMM Do YYYY'),
		fromNow: (date) => moment(date).fromNow(),
	},

	orderTasks: (list, orderByStatuses, orderByPriorities) => orderBy(
		list,
		[orderByStatuses, orderByPriorities, (task) => task?.name?.toLowerCase(), 'id'],
		['desc', 'desc', 'asc', 'desc'],
	),

	orderParents: (parents) => {
		const result = sortBy(parents.filter((task) => !task.parent), 'id');
		const byParent = groupBy(parents, (task) => task.parent?.id || 'none');
		delete byParent.none; // group by parent id, remove all the ones without parents

		// handle parent ids that aren't in the list
		let byId = keyBy(parents, 'id');
		Object.keys(byParent).forEach((id) => {
			if (!(id in byId)) {
				Array.prototype.push.apply(result, byParent[id]);
				delete byParent[id];
			}
		});

		// now go through the parents, and add any that in are the the results
		// reset, and keep going until all of the items are handled
		let next = Object.keys(byParent);
		while (next.length) {
			byId = keyBy(result, 'id');
			const toAdd = [];
			next.forEach((id) => { // eslint-disable-line no-loop-func
				if (id in byId) {
					Array.prototype.push.apply(toAdd, byParent[id]);
					delete byParent[id];
				}
			});
			Array.prototype.push.apply(result, sortBy(toAdd, 'id'));
			next = Object.keys(byParent);
		}

		return result;
	},
};

export default utils;
export { default as useQueryParams } from './useQueryParams';
export { default as queryStringParse } from './queryStringParse';
