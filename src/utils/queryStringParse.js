import queryString from 'query-string';

// memoize one value
// this way we don't have to keep re-parsing the same thing
let prevSearch = '';
let prevQueryParams = {};

export default function queryStringParse(search) {
	let queryParams = prevQueryParams;

	if (search !== prevSearch) {
		queryParams = queryString.parse(search);

		prevSearch = search;
		prevQueryParams = queryParams;
	}

	return queryParams;
}
