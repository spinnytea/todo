import utils from '.';

describe('utils', () => {
	describe('orderParents', () => {
		test('simple', () => {
			// 1 -> 2 -> 3 -> 4
			expect(utils.orderParents([
				{ id: '2', parent: { id: '1' } },
				{ id: '4', parent: { id: '3' } },
				{ id: '3', parent: { id: '2' } },
				{ id: '1', parent: undefined },
			])).toEqual([
				{ id: '1', parent: undefined },
				{ id: '2', parent: { id: '1' } },
				{ id: '3', parent: { id: '2' } },
				{ id: '4', parent: { id: '3' } },
			]);
		});

		test('two chains', () => {
			// 1 -> 2 -> 3
			// 4 -> 5
			expect(utils.orderParents([
				{ id: '5', parent: { id: '4' } },
				{ id: '2', parent: { id: '1' } },
				{ id: '4', parent: undefined },
				{ id: '3', parent: { id: '2' } },
				{ id: '1', parent: undefined },
			])).toEqual([
				{ id: '1', parent: undefined },
				{ id: '4', parent: undefined },
				{ id: '2', parent: { id: '1' } },
				{ id: '5', parent: { id: '4' } },
				{ id: '3', parent: { id: '2' } },
			]);
		});

		test('parent not in list', () => {
			// 1 -> 2 -> 3 -> 4
			expect(utils.orderParents([
				{ id: '2', parent: { id: '1' } },
				{ id: '4', parent: { id: '3' } },
				{ id: '1', parent: undefined },
			])).toEqual([
				{ id: '1', parent: undefined },
				{ id: '4', parent: { id: '3' } },
				{ id: '2', parent: { id: '1' } },
			]);
		});
	});
});