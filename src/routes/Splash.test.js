import React from 'react';
import { render, screen } from '@testing-library/react';

import Splash from './Splash';

describe('Splash', () => {
	// XXX splash makes a request to ping, which we do not respond to
	test('renders learn react link', () => {
		render(<Splash />);
		const linkElement = screen.getByText(/waiting/i);
		expect(linkElement).toBeInTheDocument();
	});
});