import React, { useCallback, useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import useTags from '../components/useTags';
import { Tags } from '../components/elements';
import TaskCards from '../components/layouts/TaskCards';
import { Alert } from '../components/mui';
import { taskActions } from '../redux';

const StyledTags = styled(Tags)`
	margin: ${(props) => props.theme.spacing(2, 1, 0, 1)};
`;

export default function Dashboard() {
	const [queryError, setQueryError] = useState();

	const refresh = useCallback(() => {
		setQueryError(undefined);
		taskActions.refresh({ inprogress: true }, () => true)
			.catch(({ message }) => { setQueryError(message); });

		// TODO let active = false; -> don't setParents
	}, []);

	useEffect(() => refresh(), [refresh]);

	const tasksOnPage = useSelector((state) => state.tasks.tasksOnPage);

	const navigate = useNavigate();
	const handleTaskSelect = useCallback((event, task) => {
		if (event) {
			event.preventDefault();
			event.stopPropagation();
		}
		navigate(`/tasks?parentId=${task.id}`);
	}, [navigate]);

	const tags = useTags();

	const handleTagSelect = useCallback((event, tag) => {
		if (event) {
			event.preventDefault();
			event.stopPropagation();
		}
		navigate(`/tasks?query=${tag}`);
	}, [navigate]);

	return (
		<div>
			<Alert message={queryError} />
			<StyledTags value={tags} readonly onSelect={handleTagSelect} />
			<TaskCards tasks={tasksOnPage} onSelect={handleTaskSelect} />
		</div>
	);
}
