import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { Fab, List, ListItem, Zoom } from '@material-ui/core';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import styled from 'styled-components';
import { isEqual } from 'lodash';

import { Checkbox, Input, Paragraph, TaskEditPanel, useEnums } from '../components';
import TaskCardsLayout from '../components/layouts/TaskCardsLayout';
import TaskTreeLayout from '../components/layouts/TaskTreeLayout';
import { AddIcon, Alert, Dialog, LinearProgress, ViewListIcon, ViewModuleIcon } from '../components/mui';
import { ButtonBar } from '../components/stylers';
import enumsService from '../services/enumsService';
import tasksService from '../services/tasksService';
import { taskActions } from '../redux';
import utils, { queryStringParse, useQueryParams } from '../utils';

const { statusCategoryClosed } = enumsService;

const StyledContainer = styled.div`
	margin: ${(props) => props.theme.spacing(1)}px;
`;

const StyledFab = styled(Fab)`
	position: fixed;
	right: ${(props) => props.theme.spacing(2)}px;
	bottom: ${(props) => props.theme.spacing(2)}px;
	z-index: 1;
`;

const StyledParent = styled(ListItem)`
	display: block;
	margin: 0;
	padding: ${(props) => props.theme.spacing(1, 2)};
`;

const stillValid = (params) => isEqual(params, queryStringParse(window.location.search));

/** REVIEW this is a monolith - how can we break it down (start with the useCallback/useMemo thingy) */
export default function TasksPage() {
	const [isRefreshing, setRefreshing] = useState(false);
	const [layout, setLayout] = useState('tree');
	const handleLayout = useCallback((event, newLayout) => { setLayout(newLayout); }, [setLayout]);

	const types = useEnums('types');
	const statuses = useEnums('statuses');
	const priorities = useEnums('priorities');

	/* List */

	const [queryParams, updateQueryParams] = useQueryParams();
	const tasksOnPage = useSelector((state) => state.tasks.tasksOnPage);
	const [parentsOnPage, setParents] = useState([]);
	const [queryError, setQueryError] = useState();
	const refresh = useCallback(() => {
		setRefreshing(true);
		setQueryError(undefined);
		const promises = Promise.all([
			tasksService.listParents(queryParams.parentId),
			taskActions.refresh(queryParams, stillValid),
		]);
		promises
			.then(([parents]) => { setParents(parents); })
			.catch(({ message }) => { setQueryError(message); })
			.finally(() => setRefreshing(false));

		// TODO let active = false; -> don't setParents
	}, [queryParams, setParents]);

	useEffect(() => refresh(), [refresh]);

	/* Selected */

	const [selectedTask, setSelectedTask] = useState(null);
	const [updateError, setUpdateError] = useState();
	const doSaveTask = useCallback((task) => {
		const disabled = !tasksService.isTaskValid(task);

		// try again later
		if (isRefreshing) {
			setTimeout(() => doSaveTask(task), utils.LONG_DELAY);
		}
		else if (task?.id && !disabled) {
			setRefreshing(true);
			setUpdateError(undefined);

			tasksService.update(task)
				.then((updatedTask) => {
					setSelectedTask((currSelected) => {
						if (currSelected?.id !== updatedTask.id) return currSelected; // if we moved to another one by the time this changes, ignore
						return updatedTask; // if we are on the same task, then update the updated
					});
				})
				.catch(({ message }) => { setUpdateError(message); })
				.finally(() => refresh());
		}
	}, [selectedTask?.id, isRefreshing, refresh]); // eslint-disable-line react-hooks/exhaustive-deps
	const updateSelectedTask = useCallback((task) => {
		setSelectedTask(task);
		doSaveTask(task);
	}, [doSaveTask]);

	// select or deselect task based on queryParams if out of sync
	// - this handles the forward and back buttons
	// - this handles refreshing the page
	useEffect(() => {
		if (!queryParams.parentId) {
			if (selectedTask) {
				setSelectedTask(null);
			}
		}
		else if (selectedTask?.id !== queryParams.parentId) {
			const task = tasksOnPage.find((t) => t.id === queryParams.parentId);
			if (task) {
				setSelectedTask(task);
			}
			else {
				const selectedTaskId = selectedTask?.id;
				// TODO can we move all the tasksService interface to redux? does that help?
				tasksService.query({ id: queryParams.parentId })
					// if the selected task's id has changed, then the user has already moved on
					.then(({ list: [t] }) => { setSelectedTask((currSelected) => (currSelected?.id !== selectedTaskId ? currSelected : t)); })
					// TODO setting task to an empty object throws some console errors
					.catch(({ message }) => { setSelectedTask({}); setUpdateError(message); });
			}
		}
	}, [queryParams, selectedTask, tasksOnPage]);

	/* New */

	const NEW_TASK = useMemo(() => ({
		description: '',
		id: 'New!',
		name: '',
		parent: selectedTask,
		priority: (priorities.find((e) => e.default) || { id: '' }).id,
		status: (statuses.find((e) => e.default) || { id: '' }).id,
		tags: [],
		type: (types.find((e) => e.default) || { id: '' }).id,
	}), [types, statuses, priorities, selectedTask]);
	const [newTask, updateNewTask] = useState({ ...NEW_TASK });
	// REVIEW this is ugly, but the best we got - since these are lazy loaded, we need to watch them and update them
	//  - if these values are loaded _after_ the modal is open, then we backfill them
	useEffect(() => { updateNewTask((nT) => ({ ...nT, priority: NEW_TASK.priority })); }, [NEW_TASK.priority]);
	useEffect(() => { updateNewTask((nT) => ({ ...nT, status: NEW_TASK.status })); }, [NEW_TASK.status]);
	useEffect(() => { updateNewTask((nT) => ({ ...nT, type: NEW_TASK.type })); }, [NEW_TASK.type]);

	const [createError, setCreateError] = useState();
	const [isNewOpen, setNewOpen] = useState(false);
	const setNewOpenTrue = useCallback(() => { updateNewTask({ ...NEW_TASK }); setNewOpen(true); }, [setNewOpen, NEW_TASK]);
	const setNewOpenFalse = useCallback(() => { setNewOpen(false); }, [setNewOpen]);

	const saveNewTask = useCallback((event) => {
		event.preventDefault();
		event.stopPropagation();

		if (isRefreshing) return;
		setRefreshing(true);
		setCreateError(undefined);

		tasksService.create(newTask)
			.then(() => { updateNewTask({ ...NEW_TASK }); setNewOpenFalse(); })
			.catch(({ message }) => { setCreateError(message); })
			.finally(() => refresh());
	}, [isRefreshing, refresh, newTask, NEW_TASK, setNewOpenFalse]);

	const handleTaskSelect = useCallback((event, task) => {
		if (event) {
			event.preventDefault();
			event.stopPropagation();
		}

		setCreateError(undefined);
		setUpdateError(undefined);
		setQueryError(undefined);

		if (!task || task.id === selectedTask?.id) {
			setSelectedTask(null);
			updateQueryParams({ parentId: undefined });
		}
		else {
			setSelectedTask(task);
			updateQueryParams({ parentId: task.id });
		}
	}, [selectedTask?.id, updateQueryParams]);

	const handleQueryParamsChange = useCallback(([name, value]) => {
		updateQueryParams({
			[name]: value,
		});
	}, [updateQueryParams]);

	/* Delete */

	const deleteSelectedTask = useCallback(() => {
		if (isRefreshing) return; // just don't
		if (!selectedTask?.id) return;

		setRefreshing(true);
		setUpdateError(undefined);

		// XXX warn about orphaned children?, or disable delete if there are children?
		//  - if we had the child count, we could use that for "forceOpen"
		//  - orphaned children are now just root items :shrug:
		tasksService.delete(selectedTask)
			.catch(({ message }) => { setUpdateError(message); })
			// select the parent, or select nothing
			.then(() => { handleTaskSelect(undefined, selectedTask.parent); })
			.finally(() => refresh());
	}, [selectedTask, isRefreshing, refresh, handleTaskSelect]);

	return (
		<StyledContainer>
			<Zoom
				in={!isNewOpen}
				unmountOnExit
			>
				<StyledFab color="primary" onClick={setNewOpenTrue}>
					<AddIcon />
				</StyledFab>
			</Zoom>
			{isNewOpen && (
				<Dialog
					open={isNewOpen}
					maxWidth="md"
				>
					<TaskEditPanel
						task={newTask}
						variant="create"
						onChange={updateNewTask}
						onSubmit={saveNewTask}
						onClose={setNewOpenFalse}
						disabled={isRefreshing}
						errorMessage={createError}
					/>
				</Dialog>
			)}
			{parentsOnPage?.length > 0 && (
				<List>
					{parentsOnPage.map((parent) => (
						<StyledParent key={parent.id} button onClick={(event) => handleTaskSelect(event, parent)}>
							<TaskEditPanel
								task={parent}
								variant="collapsed"
								disabled
								forceOpen={false}
							/>
						</StyledParent>
					))}
				</List>
			)}
			{selectedTask && (
				<div>
					<TaskEditPanel
						task={selectedTask}
						variant="edit"
						onChange={updateSelectedTask}
						onClose={(event) => handleTaskSelect(event, null)}
						onDelete={deleteSelectedTask}
						disabled={isRefreshing}
						errorMessage={updateError}
						// BUG doesn't close when we select a parent (because that query takes a moment to resolve)
						// BUG forces open if we query and find nothing - what we really want is a child count, or just "has children"
						forceOpen={tasksOnPage.length === 0}
					/>
				</div>
			)}
			<ButtonBar>
				<Input
					id="queryParams-query"
					name="query"
					autoFocus
					fullWidth={false}
					debounced="short"
					value={queryParams.query}
					valueChange={handleQueryParamsChange}
				/>
				<Checkbox
					id="queryParams-closed"
					name="closed"
					display={`Include ${statusCategoryClosed.display}`}
					value={queryParams.closed === 'true'}
					valueChange={handleQueryParamsChange}
				/>
				<ToggleButtonGroup value={layout} onChange={handleLayout} exclusive>
					<ToggleButton value="cards"><ViewModuleIcon /></ToggleButton>
					<ToggleButton value="tree"><ViewListIcon /></ToggleButton>
				</ToggleButtonGroup>
			</ButtonBar>
			<Paragraph>
				Showing {tasksOnPage.length} {tasksOnPage.length === 1 ? 'Task' : 'Tasks'}
			</Paragraph>
			<LinearProgress open={isRefreshing} />
			<Alert message={queryError} />
			{layout === 'cards' && <TaskCardsLayout tasks={tasksOnPage} onSelect={handleTaskSelect} />}
			{layout === 'tree' && <TaskTreeLayout tasks={tasksOnPage} onSelect={handleTaskSelect} />}
		</StyledContainer>
	);
}
