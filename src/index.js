import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { StylesProvider, ThemeProvider as MaterialThemeProvider } from '@material-ui/core';
import { ThemeProvider as StyledThemeProvider } from 'styled-components';

import './index.css';
import ConnectedRoot from './ConnectedRoot';
import store from './redux/store';
import reportWebVitals from './reportWebVitals';
import theme from './theme';

// globally color the body to match the style
document.body.style['background-color'] = theme.palette.background.default;

ReactDOM.render(
	// XXX material-ui: findDOMNode is deprecated in StrictMode. -- issue with Tooltip
	// <React.StrictMode>
	<Provider store={store}>
		<StylesProvider injectFirst>
			<MaterialThemeProvider theme={theme}>
				<StyledThemeProvider theme={theme}>
					<BrowserRouter>
						<ConnectedRoot />
					</BrowserRouter>
				</StyledThemeProvider>
			</MaterialThemeProvider>
		</StylesProvider>
	</Provider>,
	document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
