const path = require('path');
const config = require('lemon/src/config');

const { initialized } = require('../../server/context');
const { getTags, ensureTags } = require('../../server/tags');
const { searchTask } = require('../../server/tasks');

const FIRST_ID = '10i';
const SECOND_ID = '10p';

describe('server tags', () => {
	function initContext() {
		config.init({
			location: path.join(__dirname, '..', '..', 'test_database'),
			in_memory: true,
			memory_fallback_to_location: true,
			filename: '_settings',
		});
		return initialized;
	}

	describe('ensureTags', () => {
		test('create, update, remove, delete', async () => {
			const contexts = await initContext();
			const checkTags = () => getTags(contexts)
				.then((tags) => tags.map(({ label }) => label).sort());
			const checkTask = (id) => searchTask(contexts, { id })
				.then((tasks) => { tasks.forEach((task) => { task.tags = task.tags.sort(); }); return tasks; });
			const checkFirst = () => checkTask(FIRST_ID);
			const checkSecond = () => checkTask(SECOND_ID);

			// confirm our starting values
			await expect(checkTags()).resolves.toEqual([]);
			await expect(checkFirst()).resolves.toEqual([expect.objectContaining({ id: FIRST_ID, name: 'First!', tags: [] })]);
			await expect(checkSecond()).resolves.toEqual([expect.objectContaining({ id: SECOND_ID, name: 'Second!', tags: [] })]);

			// add a tag to t1 (create)
			await ensureTags(contexts, { id: FIRST_ID, tags: ['a', 'a', 'a'] });

			// confirm the updates
			await expect(checkTags()).resolves.toEqual(['a']);
			await expect(checkFirst()).resolves.toEqual([expect.objectContaining({ id: FIRST_ID, name: 'First!', tags: ['a'] })]);
			await expect(checkSecond()).resolves.toEqual([expect.objectContaining({ id: SECOND_ID, name: 'Second!', tags: [] })]);

			// add b tag to t1 (create)
			await ensureTags(contexts, { id: FIRST_ID, tags: ['a', 'b'] });

			// confirm the updates
			await expect(checkTags()).resolves.toEqual(['a', 'b']);
			await expect(checkFirst()).resolves.toEqual([expect.objectContaining({ id: FIRST_ID, name: 'First!', tags: ['a', 'b'] })]);
			await expect(checkSecond()).resolves.toEqual([expect.objectContaining({ id: SECOND_ID, name: 'Second!', tags: [] })]);

			// add a tag to t2 (update)
			await ensureTags(contexts, { id: SECOND_ID, tags: ['a'] });

			// confirm the updates
			await expect(checkTags()).resolves.toEqual(['a', 'b']);
			await expect(checkFirst()).resolves.toEqual([expect.objectContaining({ id: FIRST_ID, name: 'First!', tags: ['a', 'b'] })]);
			await expect(checkSecond()).resolves.toEqual([expect.objectContaining({ id: SECOND_ID, name: 'Second!', tags: ['a'] })]);

			// remove a tag to t1 (update)
			await ensureTags(contexts, { id: FIRST_ID, tags: ['b'] });

			// confirm the updates
			await expect(checkTags()).resolves.toEqual(['a', 'b']);
			await expect(checkFirst()).resolves.toEqual([expect.objectContaining({ id: FIRST_ID, name: 'First!', tags: ['b'] })]);
			await expect(checkSecond()).resolves.toEqual([expect.objectContaining({ id: SECOND_ID, name: 'Second!', tags: ['a'] })]);

			// remove b tag to t1 (delete)
			await ensureTags(contexts, { id: FIRST_ID, tags: [] });

			// confirm the updates
			await expect(checkTags()).resolves.toEqual(['a']);
			await expect(checkFirst()).resolves.toEqual([expect.objectContaining({ id: FIRST_ID, name: 'First!', tags: [] })]);
			await expect(checkSecond()).resolves.toEqual([expect.objectContaining({ id: SECOND_ID, name: 'Second!', tags: ['a'] })]);

			// swap b/a tag on t2 (create / update / delete)
			await ensureTags(contexts, { id: SECOND_ID, tags: ['c', 'b'] });

			// confirm the updates
			await expect(checkTags()).resolves.toEqual(['b', 'c']);
			await expect(checkFirst()).resolves.toEqual([expect.objectContaining({ id: FIRST_ID, name: 'First!', tags: [] })]);
			await expect(checkSecond()).resolves.toEqual([expect.objectContaining({ id: SECOND_ID, name: 'Second!', tags: ['b', 'c'] })]);

			// remove a tag to t2 (delete)
			await ensureTags(contexts, { id: SECOND_ID, tags: [] });

			// confirm the updates
			await expect(checkTags()).resolves.toEqual([]);
			await expect(checkFirst()).resolves.toEqual([expect.objectContaining({ id: FIRST_ID, name: 'First!', tags: [] })]);
			await expect(checkSecond()).resolves.toEqual([expect.objectContaining({ id: SECOND_ID, name: 'Second!', tags: [] })]);
		});
	});
});