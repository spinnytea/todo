import { fetchWrapper } from './fetchWrapper';
import theme from '../theme';

const STATUS_CATEGORIES = [
	{ id: '0', display: 'Start', accentColor: theme.palette.text.primary },
	{ id: '1', display: 'Being Addressed', accentColor: theme.palette.secondary.main },
	{ id: '2', display: 'Closed', accentColor: theme.palette.action.disabledBackground },
];

const enumsService = {
	statusCategories: STATUS_CATEGORIES,
	statusCategoryInProgress: STATUS_CATEGORIES[1],
	statusCategoryClosed: STATUS_CATEGORIES[2],

	orderBy: (enumMap, prop) => ((task) => enumMap[task[prop]].order),

	list: (variant) => fetchWrapper(fetch(`/rest/${variant}`)),

	save: (variant, item) => fetchWrapper(fetch(`/rest/${variant}/${item.id}`, {
		method: 'PUT',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify(item),
	})),

	create: (variant, item) => fetchWrapper(fetch(`/rest/${variant}`, {
		method: 'POST',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify(item),
	})),
};

export default enumsService;
