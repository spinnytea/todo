import { orderBy } from 'lodash';

import { fetchWrapper } from './fetchWrapper';

const tagsService = {
	list: () => fetchWrapper(fetch('/rest/tags'))
		.then(({ list }) => ({ list: orderBy(list, (tag) => tag.label.toLowerCase(), 'asc') })),
};

export default tagsService;
