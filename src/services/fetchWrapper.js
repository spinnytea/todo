export function fetchWrapper(promise) {
	return promise.then((response) => {
		if (response.headers.get('content-type')?.includes('application/json')) {
			if (response.status >= 400) {
				return response.json().then((json) => Promise.reject(json));
			}
			return response.json();
		}
		return response.text().then((text) => Promise.reject(new Error(text)));
	});
}

export function fetchParams(base, params = {}) {
	const url = new URL(window.location.origin + base);
	Object.keys(params).forEach((key) => url.searchParams.append(key, params[key]));
	return fetchWrapper(fetch(url));
}
