import PropTypes from 'prop-types';
import { orderBy } from 'lodash';

import { fetchParams, fetchWrapper } from './fetchWrapper';
import utils from '../utils';

/* /
// used for testing
function setPromise(result, delay) {
	return new Promise((resolve) => { setTimeout(resolve, delay); })
		.then(() => result);
}
// */

const tasksService = {
	query: (params) => fetchParams('/rest/tasks', params),

	list: (params = {}) => fetchParams('/rest/tasks?summary=true', params)
		.then(({ list }) => ({ list: orderBy(list, [(t) => t.name.toLowerCase()], ['asc']) })),

	listParents: (childId) => (childId
		? tasksService.query({ childId }).then((data) => utils.orderParents(data.list))
		: []),

	create: (task) => fetchWrapper(fetch('/rest/tasks', {
		method: 'POST',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify(task),
	})),

	update: (task) => fetchWrapper(fetch(`/rest/tasks/${task.id}`, {
		method: 'PUT',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify(task),
	})),

	delete: (task) => fetchWrapper(fetch(`/rest/tasks/${task.id}`, {
		method: 'DELETE',
		headers: { 'Content-Type': 'application/json' },
	})),

	propTypes: PropTypes.exact({
		created: PropTypes.string,
		description: PropTypes.string,
		id: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired,
		parent: PropTypes.shape({
			id: PropTypes.string.isRequired,
			name: PropTypes.string.isRequired,
			description: PropTypes.string,
			type: PropTypes.string.isRequired,
		}),
		priority: PropTypes.string.isRequired,
		status: PropTypes.string.isRequired,
		tags: PropTypes.arrayOf(PropTypes.string).isRequired,
		type: PropTypes.string.isRequired,
		updated: PropTypes.string,
	}),

	isTaskValid: (task) => (!!task.name && !!task.type && !!task.status && !!task.priority),
};

export default tasksService;
