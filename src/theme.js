import { createTheme } from '@material-ui/core/styles';
import { amber, lime, pink } from '@material-ui/core/colors';

let type = 'light';
if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
	type = 'dark';
}

const theme = createTheme({
	palette: {
		type,
		primary: { main: amber[600] },
		secondary: { main: lime[600] },
		error: pink,
	},
});

export default theme;
