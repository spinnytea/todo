import store from '../store';
import enumsService from '../../services/enumsService';

export function refresh(variant) {
	return enumsService.list(variant).then(({ list }) => {
		store.dispatch({ type: 'SET_ENUM', variant, list });
	});
}

export function relocal(variant, list) {
	store.dispatch({ type: 'SET_ENUM', variant, list });
}
