import store from '../store';
import enumsService from '../../services/enumsService';
import tagsService from '../../services/tagsService';
import tasksService from '../../services/tasksService';

export function loadAllTags() {
	return tagsService.list().then(({ list }) => {
		store.dispatch({ type: 'LOAD_ALL_TAGS', list });
	});
}

export function refresh(queryParams, stillValid) {
	const params = { ...queryParams };
	if (params.closed) {
		delete params.closed;
		// use all statuses, no need to define them
	}
	else if (params.inprogress) {
		delete params.inprogress;
		params.statuses = store.getState().enums.statuses
			.filter((e) => e.category === enumsService.statusCategoryInProgress.id)
			.map((e) => e.id);
	}
	else {
		// no status property to consider
		params.statuses = store.getState().enums.statuses
			.filter((e) => e.category !== enumsService.statusCategoryClosed.id)
			.map((e) => e.id);
	}

	return tasksService.query(params).then(({ list }) => {
		if (stillValid(queryParams)) {
			const { map: enumMap } = store.getState().enums;
			store.dispatch({ type: 'SET_TASKS_ON_PAGE', list, enumMap });
		}

		// TODO we could be smarter about when we refresh the list of tags
		//  - this is just starting somewhere (there a bigger things to do first)
		//  - we need to reload them whenever we save or delete a task / whenever the tags change
		return loadAllTags();
	});
}
