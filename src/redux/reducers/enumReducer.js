import { keyBy, orderBy } from 'lodash';

import initialState from '../initialState';

export default function enumReducer(state = initialState.enums, action) {
	switch (action.type) {
	case 'SET_ENUM':
		return {
			...state,
			[action.variant]: orderBy(action.list, ['order', 'name'], ['desc', 'asc']),
			map: {
				...state.map,
				...keyBy(action.list, 'id'),
			},
		};
	default:
		return state;
	}
}
