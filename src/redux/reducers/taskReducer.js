import initialState from '../initialState';
import enumsService from '../../services/enumsService';
import utils from '../../utils';

export default function enumReducer(state = initialState.tasks, action) {
	switch (action.type) {
	case 'SET_TASKS_ON_PAGE':
		return {
			...state,
			tasksOnPage: utils.orderTasks(action.list, enumsService.orderBy(action.enumMap, 'status'), enumsService.orderBy(action.enumMap, 'priority')),
		};
	case 'LOAD_ALL_TAGS':
		return {
			...state,
			allTags: action.list,
		};
	default:
		return state;
	}
}
