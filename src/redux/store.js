import { combineReducers, createStore } from 'redux';

import enumReducer from './reducers/enumReducer';
import taskReducer from './reducers/taskReducer';

const store = createStore(combineReducers({
	enums: enumReducer,
	tasks: taskReducer,
}));

export default store;
