export default {
	enums: {
		types: [],
		statuses: [],
		priorities: [],
		map: {}, // maps id -> enum, regardless of type
	},

	tasks: {
		allTags: [], // XXX we should have a loading state or something
		tasksOnPage: [],
	},
};
