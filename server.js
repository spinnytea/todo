const path = require('path');
const config = require('lemon/src/config');
const express = require('express');

const context = require('./server/context');

/*
set the idea database location for this server
*/

config.init({
	in_memory: false,
	location: path.join(__dirname, 'todo_database'),
	filename: '_settings',
});

/*
setup the express server
*/

const app = express();

app.use(express.static(path.join(__dirname, 'build')));

app.get('/ping', (req, res) => {
	const { ...settings } = config.settings;
	delete settings.location;
	return res.json({ message: JSON.stringify(settings) });
});

app.get('/', (req, res) => {
	res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.use('/rest', context.rest(express.Router()));

app.listen(process.env.PORT || 8080);
